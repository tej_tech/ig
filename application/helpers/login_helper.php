<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('is_login')) {

    function is_login() {
        //get main CodeIgniter object
        $ci =& get_instance();
        $s_id = $ci->session->userdata('id');
        if (!isset($s_id) && empty($s_id)) {
            redirect('admin/login', 'refresh');
        }
		
    }

}
if (!function_exists('user_track')) {

    function user_track() {
        $cis =& get_instance();
        $server_ip = $_SERVER['REMOTE_ADDR'];
        $pc_ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        $data = array(
            "server_ip" => $server_ip,
            "pc_ip" => $pc_ip
        );
        $cis->db->insert('user_track', $data);
    }
}
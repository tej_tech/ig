<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('image_upload')) {

    function image_upload($img) {
        $newwidth_icon = 100;
        $newwidth_thumb = 200;
        $newwidth_medium = 400;
        //get main CodeIgniter object

        define("MAX_SIZE", "1000");
        $res = '';
        $image = $img["name"];
        $uploadedfile = $img['tmp_name'];

        if ($image) {
            $image_ext = getimagesize($uploadedfile);
            $extension = $image_ext['mime'];
            $extension = strtolower($extension);
            if (($extension != "image/jpg") && ($extension != "image/jpeg") && ($extension != "image/png") && ($extension != "image/gif")) {
                $res = json_encode(array("status" => '0', 'err' => 'Unknown Image extension'));
            } else {
                $size = filesize($img['tmp_name']);

                if ($size > MAX_SIZE * 1024) {
                    $res = json_encode(array("status" => '0', 'err' => 'You have exceeded the size limit'));
                } else {
                    if ($extension == "image/jpg" || $extension == "image/jpeg") {
                        $src = imagecreatefromjpeg($uploadedfile);
                    } else if ($extension == "image/png") {
                        $src = imagecreatefrompng($uploadedfile);
                    } else {
                        $src = imagecreatefromgif($uploadedfile);
                    }

                    list($width, $height) = getimagesize($uploadedfile);

                    // thumb image 200px //
                    $newheight = ($height / $width) * $newwidth_thumb;
                    $tmp_thumb = imagecreatetruecolor($newwidth_thumb, $newheight);
                    imagecopyresampled($tmp_thumb, $src, 0, 0, 0, 0, $newwidth_thumb, $newheight, $width, $height);

                    // medium image 400px //
                    $newheight1 = ($height / $width) * $newwidth_medium;
                    $tmp_medium = imagecreatetruecolor($newwidth_medium, $newheight1);
                    imagecopyresampled($tmp_medium, $src, 0, 0, 0, 0, $newwidth_medium, $newheight1, $width, $height);

                    // icon image 100px //
                    $newheight1 = ($height / $width) * $newwidth_icon;
                    $tmp_icon = imagecreatetruecolor($newwidth_icon, $newheight1);
                    imagecopyresampled($tmp_icon, $src, 0, 0, 0, 0, $newwidth_icon, $newheight1, $width, $height);

                    $image_name = time() . $img['name'];

                    $filename_thumb = FCPATH . 'assets/process_image/thumb/' . $image_name; //200px
                    $filename_medium = FCPATH . 'assets/process_image/medium/' . $image_name; //400px
                    $filename_icon = FCPATH . 'assets/process_image/icon/' . $image_name; //100px
                    $filename_original = FCPATH . 'assets/process_image/original/' . $image_name; //original

                    move_uploaded_file($uploadedfile, $filename_original);
                    imagejpeg($tmp_thumb, $filename_thumb, 100);
                    imagejpeg($tmp_medium, $filename_medium, 100);
                    imagejpeg($tmp_icon, $filename_icon, 100);

                    imagedestroy($src);
                    imagedestroy($tmp_thumb);
                    imagedestroy($tmp_medium);
                    imagedestroy($tmp_icon);
                    $res = json_encode(array("status" => '1', 'err' => $image_name));
                    return $res;
                }
            }
        }
//If no errors registred, print the success message
        // mysql_query("update SQL statement ");
    }

}

if (!function_exists('image_url')) {
    function image_url($image_name,$image_type){
        $im =& get_instance();
        $r = '';
        if($image_type == "original"){
            $r = $im->config->item('original').$image_name;
        }else if($image_type == "medium"){
            $r =  $im->config->item('medium').$image_name;
        }else if($image_type == "thumb"){
            $r =  $im->config->item('thumb').$image_name;
        }else if($image_type == "icon"){
            $r =  $im->config->item('icon').$image_name;
        }
        return $r;
    }
}
?>
<?php

class Login_model extends CI_Model {

    function check_login($email, $password) {
        $this->db->where(array('email_id' => $email, 'password' => $password));
        return $this->db->count_all_results('admin_login');
    }
    
    function get_admin($email){
        $this->db->select('id,email_id,role,image_url,name,last_login');
        $this->db->where(array('email_id' => $email));
        return $this->db->get('admin_login')->result_array()[0];
    }
    
    function update_last_login($email){
        $time = time();
       // $this->db->update('mytable', $data, array('id' => $id));
        $this->db->where(array('email_id' => $email));
        $this->db->set(array('last_login'=> $time));
        $this->db->update('admin_login');
    }

}

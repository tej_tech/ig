<?php
class Mapping_model extends CI_Model {
    
    function save_mapping($data) {
        $qry = $this->db->insert('brand_attr_map', $data);
        //return $this->db->insert_id();
		if ($qry) {
			return TRUE; // Or do whatever you gotta do here to raise an error
		} else {
			return FALSE;
		}
    }
	function edit_mapping($data,$id) {
		
        $this->db->set($data);
		$this->db->where(array("id" => $id));
        $qry = $this->db->update('brand_attr_map');
		if ($qry) {
			return TRUE; // Or do whatever you gotta do here to raise an error
		} else {
			return FALSE;
		}
		//return 
	}
    
	
	function get_mapping($mapping=0) {
        $this->db->select("*");
        $this->db->where(array("delete_status" => "0","id"=>$mapping));
        return $this->db->get('brand_attr_map')->first_row();
    }
	
    function delete_update($id) {
        $this->db->set(array("delete_status" => "1"));
		$this->db->where(array("id" => $id));
        $qry = $this->db->update('brand_attr_map');
		
        
		//echo $this->db->last_query();
		if ($qry) {
			return TRUE; // Or do whatever you gotta do here to raise an error
		} else {
			return FALSE;
		}
    }
    
}

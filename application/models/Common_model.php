<?php
class Common_model extends CI_Model {

    function get_fruits($srch='') {
        $this->db->select("*");
        $this->db->where(array("status" => "1"));
		if($srch!='')
		 $this->db->like('name',$srch);
        //$this->db->order_by("id", "desc");
        return $this->db->get('master_fruits')->result_array();
    }

    function get_brand() {
        $this->db->select("id,b_name,b_code");
        $this->db->where(array("delete_status" => "0"));
        $this->db->order_by("id", "desc");
        return $this->db->get('brand_items')->result_array();
    }

    function get_brand_attr() {
        $this->db->select("id,attr_name,attr_desc");
        $this->db->where(array("delete_status" => "0"));
        return $this->db->get("brand_attr")->result_array();
    }
	
	function getMappingList($srch='') {
        $this->db->select("m.id,m.unit_price_s,i.b_name,f.name,b.attr_name,m.unit_price1,m.unit_price2,m.unit_count");
		$this->db->from('brand_attr_map m');
		$this->db->join('brand_attr b', 'm.attr_id = b.id');
		$this->db->join('master_fruits f', 'm.fruit_id = f.id');
		$this->db->join('brand_items i', 'm.brand_id = i.id');
		$this->db->where(array("m.delete_status" => "0"));
		$this->db->order_by("m.fruit_id,m.brand_id", "asc");
		//$this->db->order_by("m.brand_id", "asc");
		$query = $this->db->get();
		return $query->result_array();		
    }
	
	function get_store() {
        $this->db->select("id,store_name");
        return $this->db->get("stores")->result_array();
    }
	function get_storeName($store=0) {
		return $this->db->query("SELECT store_name FROM stores WHERE id=$store")->row()->store_name;
        //$this->db->select("id,store_name");
        //return $this->db->get("stores")->result_array();
    }
}

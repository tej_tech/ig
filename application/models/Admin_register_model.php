<?php

class Admin_register_model extends CI_Model {

    function check_email($email) {
        $this->db->where('email_id', $email);
        $this->db->from('admin_login');
        return $this->db->count_all_results();
    }

    function save($data) {
        $this->db->insert('admin_login', $data);
    }

}

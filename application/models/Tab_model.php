<?php

Class Tab_model extends CI_Model {

    function get_store() {
        $this->db->select("id,store_name");
        return $this->db->get("stores")->result_array();
    }

    function get_tab() {
        $this->db->select("*");
        $this->db->where(array("delete_status" => "0"));
        return $this->db->get("tabs")->result_array();
    }

    function get_storeName($store = 0) {
        $this->db->select("id,store_name");
        $this->db->from("stores");
        $this->db->where(array("id" => $store));
        //$this->db->get();
        //echo $this->db->last_query();
        $arr = $this->db->get()->result_array();
        //print_R($arr);
        if (sizeof($arr) > 0)
            return $arr[0]['store_name'];
    }

    function get_tab_search($srch = '') {
        $this->db->select("id,store_id,model_code,price,purchase_date");
        $this->db->where(array("delete_status" => "0"));
        if ($srch != '') {
            $this->db->like('model_code', $srch);
        }
        //$this->db->order_by("id", "desc");
        return $this->db->get('tabs')->result_array();
    }

    function get_tab_id($id) {
        $this->db->select("id,store_id,model_code,price,purchase_date");
        $this->db->where(array("delete_status" => "0", "id" => $id));
        return $this->db->get('tabs')->result_array();
    }

    function count_tab($data) {
        $this->db->where($data);
        return $this->db->count_all_results('tabs');
    }

    function save_tab_attr($data) {
        $this->db->insert('tabs', $data);
        return $this->db->insert_id();
    }

    function _update($id, $d) {
        $this->db->where(array("id" => $id));
        $this->db->set($d);
        $this->db->update('tabs');
    }

    function delete_update($id) {
        $this->db->where(array("id" => $id));
        $this->db->set(array("delete_status" => "1"));
        $this->db->update('tabs');
    }

}

?>
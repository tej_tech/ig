<?php

class Admin_model extends CI_Model {

    function get_user_count() {
        $this->db->select('*');
        return $this->db->get('users')->result_array();
    }

    function get_video_count() {
        $this->db->select('count(*) as v_count');
        return $this->db->count_all_results('video_list');
    }
    
    function get_video_view_count() {
        $this->db->select('count(*) as vv_count');
        return $this->db->count_all_results('video_stats');
    }

    function get_comment_like_count() {
        $this->db->select('count(*) as blog_count, sum(like_count) as blog_like_count, sum(comment_count) as comment_like_count');
        return $this->db->get('blog')->result_array();
    }

}

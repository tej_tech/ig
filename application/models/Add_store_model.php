<?php

class Add_store_model extends CI_Model {

    function get_state() {
        $this->db->select("state_id,state_name");
        return $this->db->get("master_state")->result_array();
    }

    function get_cd($id) {
        $this->db->select("location_id,location_name");
        $this->db->where(array("state_id" => $id));
        return $this->db->get("master_location")->result_array();
    }
    
    function check_store($sname ,$location){
        $data = array("store_name" => $sname,"location" => $location);
        $this->db->where($data);
        return $this->db->count_all_results('stores');
    }
    
    function add_store($data){
        $this->db->insert('stores', $data);
    }

}

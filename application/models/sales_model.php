<?php
class Sales_model extends CI_Model {

    function validate_fruit_name($bname) {
        $this->db->where(array("name" => $bname, "status" => "1"));
        return $this->db->count_all_results('master_fruits');
    }

    function save_sales($data) {
        $this->db->insert('sales_list', $data);
        return $this->db->insert_id();
    }

    function save_salesList($data) {
        $this->db->insert('sales_list_detail', $data);
        return $this->db->insert_id();
    }

    function getSalesList($srch = '') {
        $this->db->select("*");
        $this->db->from('sales_list');
        $this->db->where(array("status" => "0"));
        if ($srch != '')
            $this->db->where(array("sales_date" => $srch));
        $this->db->order_by("sales_date", "desc");
        //$this->db->order_by("m.brand_id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    function validateSaleDate($date) {
        $qry = $this->db->query("SELECT * FROM sales_list WHERE sales_date='" . $date . "'");
        if ($qry->num_rows() > 0)
            return false;
        else
            return true;
    }

    /* function getSalesList($srch='') {
      $this->db->select("m.id,m.unit_price_s,i.b_name,f.name,b.attr_name,m.unit_price1,m.unit_price2,m.unit_count");
      $this->db->from('brand_attr_map m');
      $this->db->join('brand_attr b', 'm.attr_id = b.id');
      $this->db->join('master_fruits f', 'm.fruit_id = f.id');
      $this->db->join('brand_items i', 'm.brand_id = i.id');
      $this->db->where(array("m.delete_status" => "0"));
      $this->db->order_by("m.fruit_id,m.brand_id", "asc");
      //$this->db->order_by("m.brand_id", "asc");
      $query = $this->db->get();
      return $query->result_array();
      } */

    function delete_update($id) {
        $this->db->where(array("id" => $id));
        $this->db->set(array("delete_status" => "1"));
        $this->db->update('brand_items');
    }

    function validate_fruit_update($id, $name) {
        $this->db->where(array("name" => $name, "status" => "1"));
        $this->db->where_not_in("id", $id);
        return $this->db->count_all_results('master_fruits');
    }

    function get_sales_data_id($id) {
        $this->db->select('a.id,a.sales_id,a.map_id,a.price1,a.price2,b.attr_name,f.name,i.b_name');
        $this->db->where(array("a.sales_id" => $id));
        $this->db->from('sales_list_detail as a');
        $this->db->join('brand_attr_map m', 'a.map_id = m.id');
        $this->db->join('brand_attr b', 'b.id = m.attr_id');
        $this->db->join('master_fruits f', 'm.fruit_id = f.id');
        $this->db->join('brand_items i', 'm.brand_id = i.id');
        return $this->db->get()->result_array();
    }

    function get_sales_list_edit($id) {
        $this->db->select('a.map_id,a.price1,a.price2,m.sales_date');
        $this->db->where(array("a.sales_id" => $id));
        $this->db->from('sales_list_detail as a');
        $this->db->join('sales_list m', 'm.id = a.sales_id', 'right');
        return $this->db->get()->result_array();
    }

    function update_sales_list($sales_id, $map_id, $array) {
        $this->db->where(array("sales_id" => $sales_id, "map_id" => $map_id));
        $this->db->set($array);
        $this->db->update('sales_list_detail');
    }

    function delete_list_arr($sale_id, $map_id) {
        $this->db->where(array("sales_id" => $sale_id, "map_id" => $map_id));
        $this->db->delete('sales_list_detail');
    }
	
	function getSalesArrId($id) {
        $this->db->select('map_id');
        $this->db->from('sales_list_detail');
        $this->db->where('sales_id',$id);
        $resultArr = $this->db->get()->result_array();
		$retArr = array();
		foreach($resultArr as $arr){
			$retArr[] = $arr['map_id'];
		}
		
		return $retArr;
    }
	
}

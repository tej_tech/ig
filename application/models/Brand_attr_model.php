<?php

class Brand_attr_model extends CI_Model {

    function get_brand_attr() {
        $this->db->select("id,attr_name,attr_desc");
        $this->db->where(array("delete_status" => "0"));
        return $this->db->get("brand_attr")->result_array();
    }

    function validate_brand_name($aname) {
        $this->db->where(array("attr_name" => $aname, "delete_status" => "0"));
        return $this->db->count_all_results('brand_attr');
    }

    function save_brand_attr($data) {
        $this->db->insert('brand_attr', $data);
        return $this->db->insert_id();
    }

    function get_brand_id($id) {
        $this->db->select("id,attr_name,attr_desc");
        $this->db->where(array("delete_status" => "0", "id" => $id));
        $this->db->order_by("id", "desc");
        return $this->db->get('brand_attr')->result_array();
    }

    function delete_update($id) {
        $this->db->where(array("id" => $id));
        $this->db->set(array("delete_status" => "1"));
        $this->db->update('brand_attr');
    }

    function _update($id, $d) {
        $this->db->where(array("id" => $id));
        $this->db->set(array("attr_desc" => $d));
        $this->db->update('brand_attr');
    }

    function get_fruits_attr($srch = '') {
        $this->db->select("id,attr_name,attr_desc");
        $this->db->where(array("delete_status" => "0"));
        if ($srch != ''){
            $this->db->like('attr_name', $srch);
        }
        //$this->db->order_by("id", "desc");
        return $this->db->get('brand_attr')->result_array();
    }

}

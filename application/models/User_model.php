<?php
class User_model extends CI_Model {

    function save($data) {
        $this->db->insert('admin_login', $data);
		return $this->db->insert_id();
    }
	function save_detail($data) {
        $this->db->insert('admin_detail', $data);
		return $this->db->insert_id();
    }
	
	function update_user($data,$id) {
		$this->db->where(array("id" => $id));
        $this->db->set($data);
        $this->db->update('admin_login');
    }
	function update_user_detail($data,$id) {
        /*$this->db->insert('admin_detail', $data);
		return $this->db->insert_id();*/
		$this->db->where(array("admin_id" => $id));
        $this->db->set($data);
        $this->db->update('admin_detail');
		//echo $this->db->last_query();exit;
    }
	
	function getUsers($roles=''){
		$this->db->select("a.id,a.name,a.role,a.email_id,d.mobile,d.pin,d.store,d.address");
		$this->db->from('admin_login a');
		$this->db->join('admin_detail d', 'a.id = d.admin_id','left');
		if($roles!='')
		$this->db->where('a.role',$roles);
		$this->db->order_by("a.date_added", "desc");
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function getUser($user=0){
		$this->db->select("a.id,a.name,a.role,a.email_id,d.mobile,d.pin,d.store,d.address");
		$this->db->from('admin_login a');
		$this->db->join('admin_detail d', 'a.id = d.admin_id','left');
		
		$this->db->where('a.id',$user);
		$this->db->order_by("a.date_added", "desc");
		$query = $this->db->get();
		return $query->result_array();
	}
		
}

<?php

Class Stock_management_model extends CI_Model {

    function get_list() {
        $this->db->select('id,map_id,quantity');
        $this->db->where('status', '0');
        $this->db->order_by('id', 'desc');
        return $this->db->get('stock_management')->result_array();
    }

    function get_list_drop() {
        $this->db->select('b.id,c.name,d.attr_name,e.b_name');
        $this->db->from('brand_attr_map as b');
        $this->db->join('master_fruits as c', 'b.fruit_id = c.id', 'inner');
        $this->db->join('brand_attr as d', 'b.attr_id = d.id', 'innner');
        $this->db->join('brand_items as e', 'b.brand_id = e.id', 'inner');
        return $this->db->get()->result_array();
    }

    function get_list_drop_id($i) {
        $this->db->select('a.id,a.map_id,c.name,d.attr_name,e.b_name,a.quantity');
        $this->db->where(array("a.map_id" => $i));
        $this->db->from('stock_management as a');
        $this->db->join('brand_attr_map as b', 'a.map_id = b.id', 'right');
        $this->db->join('master_fruits as c', 'b.fruit_id = c.id', 'right');
        $this->db->join('brand_attr as d', 'b.attr_id = d.id', 'inner');
        $this->db->join('brand_items as e', 'b.brand_id = e.id', 'inner');
        return $this->db->get()->result_array();
    }

    function get_list_drop_ids($i) {
        $this->db->select('a.id,a.map_id,c.name,d.attr_name,e.b_name,a.quantity');
        $this->db->where_not_in(array("a.id" => $i));
        $this->db->from('stock_management as a');
        $this->db->join('brand_attr_map as b', 'a.map_id = b.id', 'right');
        $this->db->join('master_fruits as c', 'b.fruit_id = c.id', 'right');
        $this->db->join('brand_attr as d', 'b.attr_id = d.id', 'inner');
        $this->db->join('brand_items as e', 'b.brand_id = e.id', 'inner');
        return $this->db->get()->result_array();
    }

    function validate_stock($arr) {
		$this->db->select('id,map_id,quantity');
        $this->db->where($arr);
        return $this->db->get('stock_management')->result_array();
		
        /*$this->db->where($arr);
        return $this->db->count_all_results('stock_management');*/
    }

    function save_stock_management($data) {
        $this->db->insert('stock_management', $data);
         $this->save_stock_detail($data);
    }

    function delete($id) {
        $this->db->where(array("map_id" => $id));
        $this->db->set(array("status" => "1"));
        $this->db->update('stock_management');
        echo $id;
    }

    function get_by_id($id) {
        $this->db->select('id,map_id,quantity');
        $this->db->where(array('status' => '0', "map_id" => $id));
        return $this->db->get('stock_management')->result_array();
    }

    function update_stock($id,$f) {
        $this->db->query("UPDATE `stock_management` SET `quantity` = `quantity`+$f WHERE `map_id` = $id AND status='0'");
        /*$this->db->set(array("quantity" => quantity$f));'
        $this->db->where(array("map_id" => $id));
		$this->db->update('stock_management');
		echo $this->db->last_query();*/
    }
	function edit_stock($id,$f) {
        $this->db->query("UPDATE `stock_management` SET `quantity` = $f WHERE `map_id` = $id AND status='0'");
        /*$this->db->set(array("quantity" => quantity$f));
        $this->db->where(array("map_id" => $id));
		$this->db->update('stock_management');
		echo $this->db->last_query();*/
    }
    
    function save_stock_detail($data){
        $this->db->insert('stock_detail',$data);
    }

}

<?php

class Fruits_model extends CI_Model {

    function validate_fruit_name($bname) {
        $this->db->where(array("name" => $bname, "status" => "1"));
        return $this->db->count_all_results('master_fruits');
    }

    function save_fruit($data) {
        $this->db->insert('master_fruits', $data);
        return $this->db->insert_id();
    }

    function get_fruits($srch = '') {
        $this->db->select("*");
        $this->db->where(array("status" => "1"));
        if ($srch != '')
            $this->db->like('name', $srch);
        //$this->db->order_by("id", "desc");
        return $this->db->get('master_fruits')->result_array();
    }

    function get_fruit_id($id) {
        $this->db->select("*");
        $this->db->where(array("status" => "1", "id" => $id));
        return $this->db->get('master_fruits')->result_array();
    }

    function delete_update($id) {
        $this->db->where(array("id" => $id));
        $this->db->set(array("delete_status" => "1"));
        $this->db->update('brand_items');
    }

    function validate_fruit_update($id, $name) {
        $this->db->where(array("name" => $name, "status" => "1"));
        $this->db->where_not_in("id" ,$id);
        return $this->db->count_all_results('master_fruits');
    }

    function _update($id, $d) {
        $this->db->where(array("id" => $id));
        $this->db->set(array("name" => $d));
        $this->db->update('master_fruits');
    }

}

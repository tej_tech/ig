<?php

class Item_brand_model extends CI_Model {

    function validate_brand_name($bname) {
        $this->db->where(array("b_name" => $bname, "delete_status" => "0"));
        return $this->db->count_all_results('brand_items');
    }

    function save_brand($data) {
        $this->db->insert('brand_items', $data);
        return $this->db->insert_id();
    }

    function get_brand() {
        $this->db->select("id,b_name,b_code");
        $this->db->where(array("delete_status" => "0"));
        $this->db->order_by("id", "desc");
        return $this->db->get('brand_items')->result_array();
    }

    function get_brand_id($id) {
        $this->db->select("id,b_name,b_code");
        $this->db->where(array("delete_status" => "0","id" => $id));
        $this->db->order_by("id", "desc");
        return $this->db->get('brand_items')->result_array();
    }

    function delete_update($id) {
        $this->db->where(array("id" => $id));
        $this->db->set(array("delete_status" => "1"));
        $this->db->update('brand_items');
    }

    function _update($id, $d) {
        $this->db->where(array("id" => $id));
        $this->db->set(array("b_code" => $d));
        $this->db->update('brand_items');
    }

}

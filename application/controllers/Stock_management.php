<?php

Class Stock_management extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('stock_management_model', 'sm');
    }

    function index() {
        $a = $this->sm->get_list();
        $l = $this->sm->get_list_drop();
        $rr = array();
        foreach ($l as $e) {
            $rr[$e['id']] = $e;
        }

        $c = array();
        foreach ($a as $key => $b) {
            $c[$key]['map_id'] = $b['map_id'];
            $c[$key]['quantity'] = $b['quantity'];
            $c[$key]['fruit_name'] = $rr[$b['map_id']]['name'];
            $c[$key]['attr_name'] = $rr[$b['map_id']]['attr_name'];
            $c[$key]['brand_name'] = $rr[$b['map_id']]['b_name'];
        }

        $r = array();
        foreach ($l as $e) {
            $r[$e['name']][] = $e;
        }
        $data['list'] = $c;
        $data['drop'] = $r;
        $this->load->view('admin/common/header');
        $this->load->view('admin/operation/stock_management/landing_page', $data);
        $this->load->view('admin/common/footer');
    }

    function validate_entry($id) {
        $arr = array("map_id" => $id);
        return $this->sm->validate_stock($arr);
    }

    function save() {
        if (!empty($this->input->post('map_id')) && !empty($this->input->post('quantity'))) {
            $f = $this->validate_entry($this->input->post('map_id'));
            if (sizeof($f) == 0) {
                $array = array(
                    "map_id" => $this->input->post('map_id'),
                    "quantity" => $this->input->post('quantity')
                );
                $this->sm->save_stock_management($array);

                $fr = $this->sm->get_list_drop_id($this->input->post('map_id'))[0];
                echo json_encode(array("err" => 0, "msg" => "<font color=\"green\">Stock added successfully</font>", "fruit" => $fr['name'], "attr" => $fr['attr_name'], "brand" => $fr['b_name'], "quantity" => $fr['quantity'], "id" => $fr['map_id']));
            } else {
                $array = array(
                    "map_id" => $this->input->post('map_id'),
                    "quantity" => $this->input->post('quantity')
                );
                $this->sm->update_stock($this->input->post('map_id'), $this->input->post('quantity'));
                $fr = $this->sm->get_list_drop_id($this->input->post('map_id'))[0];
                $this->sm->save_stock_detail(array("map_id" => $this->input->post('map_id'), "quantity" => $fr['quantity']));
                echo json_encode(array("err" => 0, "msg" => "<font color=\"green\">Stock updated successfully</font>", "fruit" => $fr['name'], "attr" => $fr['attr_name'], "brand" => $fr['b_name'], "quantity" => $fr['quantity'], "id" => $this->input->post('map_id')));
                //echo json_encode(array("err" => 2, "msg" => "<font color=\"red\">Quantity against band has already added</font>"));
            }
        } else {
            echo json_encode(array("err" => 1, "msg" => "<font color=\"red\">Please fill all fields</font>"));
        }
    }

    function get_by_id() {
        if (!empty($this->input->post('id'))) {
            $d = $this->sm->get_by_id($this->input->post('id'));
            echo json_encode($d[0]);
        }
    }

    function edit() {
        if (!empty($this->input->post('map_id')) && !empty($this->input->post('quantity'))) {
            $this->sm->edit_stock($this->input->post('map_id'), $this->input->post('quantity'));
            $this->sm->save_stock_detail(array("map_id" => $this->input->post('map_id'), "quantity" => $this->input->post('quantity')));
            echo json_encode(array("err" => 0, "msg" => "<font color=\"green\">updated Successfully</font>","quantity" => $this->input->post('quantity'),"map_id" => $this->input->post('map_id')));
        } else {
            echo json_encode(array("err" => 1, "msg" => "<font color=\"red\">Please fill all fields</font>"));
        }
    }

    function delete() {
        if (!empty($this->input->post("id"))) {
            echo $this->sm->delete($this->input->post("id"));
        }
    }

}

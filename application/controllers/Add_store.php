<?php

class Add_store extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('add_store_model', 'as');
    }

    function index() {
        $data['state'] = $this->as->get_state();
        $data['cd'] = $this->as->get_cd(1);
        $this->load->view('admin/common/header');
        $this->load->view('admin/admin_register/add_store', $data);
        $this->load->view('admin/common/footer');
    }

    function validate_add_store() {
        if (!empty($this->input->post('store_name')) && !empty($this->input->post('email_s')) && !empty($this->input->post('mob_s')) && !empty($this->input->post('address_s')) && !empty($this->input->post('state_rep')) && !empty($this->input->post('cd_s')) && !empty($this->input->post('pin_s'))) {
            $count = $this->as->check_store($this->input->post('store_name'), $this->input->post('cd_s'));
            if ($count > 0) {
                echo 1;
            } else {
                $data = array(
                    "email_id" => $this->input->post('email_s'),
                    "mobile" => $this->input->post('mob_s'),
                    "street_s" => $this->input->post('address_s'),
                    "state_s" => $this->input->post('state_rep'),
                    "pin" => $this->input->post('pin_s'),
                    "added_by" => $this->session->userdata('id'),
                    "store_name" => $this->input->post('store_name'),
                    "location" => $this->input->post('cd_s')
                );
                $this->as->add_store($data);
            }
        } else {
            echo 0;
        }
    }

    function get_location() {
        if (!empty($this->input->post('id'))) {
            $f = $this->as->get_cd($this->input->post('id'));
            foreach ($f as $c) {
                ?>
                <option value="<?php echo $c['location_name']; ?>"><?php echo $c['location_name']; ?></option>
                <?php
            }
        }
    }

}

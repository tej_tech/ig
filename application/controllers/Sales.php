<?php

class Sales extends CI_Controller {

    function __construct() {
        parent::__construct();
        is_login();
        $this->load->model('sales_model', 'sale');
        $this->load->model('common_model', 'common');
    }

    function index() {
        $this->lists();
    }

    function lists() {
        $data['msg'] = '';
        $this->load->view('admin/common/header');
        $this->load->view('admin/operation/sales_list/landing_page', $data);
        $this->load->view('admin/common/footer');
    }

    function add() {
        $data['msg'] = '';
        $this->load->helper('form');

        if ($this->input->post('action') == 'newSale') {
            $unit_price1 = $this->input->post("unit_price1");
            if (null != $this->input->post("unit_price2"))
                $unit_price2 = $this->input->post("unit_price2");
            else
                $unit_price2 = array();

            $this->db->trans_start();
            $sales_list['sales_date'] = $this->input->post("sales_date");
            $sales_list['created_by'] = $this->session->userdata('id');
            if ($this->sale->validateSaleDate($sales_list['sales_date'])) {
                $sale = $this->sale->save_sales($sales_list);
                foreach ($this->input->post('addThis') as $mapid) {
                    if ($unit_price1[$mapid] > 0) {
                        //save_salesList
                        $sales_detail['sales_id'] = $sale;
                        $sales_detail['map_id'] = $mapid;
                        $sales_detail['price1'] = $unit_price1[$mapid];
                        if (isset($unit_price2[$mapid]))
                            $sales_detail['price2'] = $unit_price2[$mapid];
                        else
                            $sales_detail['price2'] = 0;
                        $this->sale->save_salesList($sales_detail);
                    }
                    else {
                        $data['msg'] = 'Price should be greater than 0';
                        break;
                    }
                    //echo $unit_price1[$mapid].'<br>';	
                }
                //$this->db->trans_complete();

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $data['msg'] = 'There is an error, Please try again!';
                } else {
                    $this->db->trans_commit();
                    redirect('sales/lists');
                    exit;
                }
            } else
                $data['msg'] = 'Date already exist';
            //sales_date
        }
        $data['maplist'] = $this->common->getMappingList();
        $this->load->view('admin/common/header');
        $this->load->view('admin/operation/sales_list/add', $data);
        $this->load->view('admin/common/footer');
    }

    function getSalesList() {
        if ($this->input->post('salesdate'))
            $searchStr = $this->input->post('salesdate');
        else
            $searchStr = '';
        $data['sales'] = $this->sale->getSalesList($searchStr);
        $this->load->view('admin/operation/sales_list/salesList', $data);
    }

    function get_sales_id() {
        $id = $this->input->post('id');
        $data['l'] = $this->sale->get_sales_data_id($id);
        echo $this->load->view('admin/operation/sales_list/salesList_id', $data, true);
    }

    function edit_sales_list() {
        if (!empty($this->input->post('id'))) {
            $id = $this->input->post('id');
            $data['maplist'] = $this->common->getMappingList();
            $data['xm'] = $xm = $this->sale->get_sales_list_edit($id);
            $data['f'] = array_column($xm, 'map_id');
            $data['ids'] = $id;
            echo $this->load->view('admin/operation/sales_list/salesList_edit', $data, true);
        }
    }

    function edit_sales_list_id() {
        $d = $this->input->post('addThis');
        $e = $this->input->post('unit_price1');
        $f = $this->input->post('unit_price2');
        $g = $this->input->post('sales_date');
        $id = $this->input->post('ids');
        //$h = explode(',', $this->input->post('arr'));
		$h = $this->sale->getSalesArrId($id);
        $a = $b = $c = array();
        $xm = $this->sale->get_sales_list_edit($id);
        $fd = array_column($xm, 'map_id');
        foreach($fd as $r){
             if (!in_array($r, $d)) {
                 
                 //echo $r." ";
                 
                 $this->sale->delete_list_arr($id,$r);
             }
        }
        foreach ($d as $map_id) {
            $ar = array(
                "map_id" => $map_id,
                "price1" => $e[$map_id],
                "price2" => $f[$map_id]
            );
            if (in_array($map_id, $h)) {
                $this->sale->update_sales_list($id, $map_id, $ar);
            } else {
                $ar['sales_id'] = $id;
                $this->sale->save_salesList($ar);
            }
        }
    }

}

<?php

Class Item_brand extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('item_brand_model', 'ib');
    }

    function index() {
        $data['brand'] = $this->ib->get_brand();
        $this->load->view('admin/common/header');
        $this->load->view('admin/operation/item_brand/landing_page', $data);
        $this->load->view('admin/common/footer');
    }

    function validate_brand_name() {
        if (!empty($this->input->post('bname'))) {
            $c = $this->ib->validate_brand_name($this->input->post('bname'));
            if ($c == 0) {
                echo 0;
            } else {
                echo 1;
            }
        }
    }

    function save_item_brand() {
        if (!empty($this->input->post('bname')) && !empty($this->input->post('bcode'))) {
            $c = $this->ib->validate_brand_name($this->input->post('bname'));
            $brand_array = array(
                "b_name" => $this->input->post('bname'),
                "b_code" => $this->input->post('bcode'),
                "created_by" => $this->session->userdata('id'),
                "updated_by" => $this->session->userdata('id'),
                "updated_date" => date("Y-m-d H:s:i", time())
            );
            if ($c == 0) {
                $d = $this->ib->save_brand($brand_array);
                $g = "<tr id=\"brandremove_$d\">
                <td id=\"en_$d\">". $this->input->post('bname') . "</td>
                <td id=\"ec_$d\">". $this->input->post('bcode') . "</td>
                <td>
                
                <a href = \"#\" title = \"Edit\"  data-toggle=\"modal\" data-target=\"#myModal1\"  id=\"upbr_$d\" class = \"btn btn-xs btn-outline update_brand btn-success add-tooltip\"><i class = \"fa fa-pencil\"></i></a>
                <a href = \"#\" title = \"Delete\" id=\"bdel_$d\" class = \"btn btn-xs brand_delete btn-outline btn-danger add-tooltip\"><i class = \"fa fa-times\"></i></a>
                </td>
                </tr>";
                echo json_encode(array("brand" => $g, "msg" => "<font color=\"green\">Brand name added successfully</font>", "id" => 0));
            } else {
                echo json_encode(array("msg" => "<font color=\"red\">Brand name already used</font>", "id" => 1));
            }
        } else {
            echo json_encode(array("msg" => "<font color=\"red\">Fill all fields properly</font>", "id" => 1));
        }
    }
    
    function get_brand_id(){
         if (!empty($this->input->post('id'))) {
           $c = $this->ib->get_brand_id($this->input->post('id'));
          echo json_encode(array("id" =>  $c[0]['id'],"bname" => $c[0]['b_name'], "bcode" => $c[0]['b_code']));
        }
    }

    function update_brand() {
        if (!empty($this->input->post('id_brand')) && !empty($this->input->post('bcode'))) {
            $this->ib->_update($this->input->post('id_brand'), $this->input->post('bcode'));
            echo json_encode(array("id" => $this->input->post('id_brand'), "bcode" => $this->input->post('bcode')));
        }
    }

    function delete_brand() {
        if (!empty($this->input->post('id'))) {
            $this->ib->delete_update($this->input->post('id'));
            echo $this->input->post('id');
        }
    }

}

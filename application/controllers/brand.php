<?php

class Brand extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('item_brand_model', 'ib');
    }

    function index() {
        $this->load->view('admin/common/header');
        $this->load->view('admin/operation/item_brand/brand');
        $this->load->view('admin/common/footer');
    }

    function validate_brand_name() {
        if (!empty($this->input->post('bname'))) {
            $c = $this->ib->validate_brand_name($this->input->post('bname'));
            if ($c == 0) {
                echo 0;
            } else {
                echo 1;
            }
        }
    }

    function save_item_brand() {
        if (!empty($this->input->post('bname')) && !empty($this->input->post('bcode'))) {
            $c = $this->ib->validate_brand_name($this->input->post('bname'));
            $brand_array = array(
                "b_name" => $this->input->post('bname'),
                "b_code" => $this->input->post('bcode'),
                "created_by" => $this->session->userdata('id'),
                "updated_by" => $this->session->userdata('id'),
                "updated_date" => date("Y-m-d H:s:i", time())
            );
            if ($c == 0) {
                $this->ib->save_brand($brand_array);
                echo json_encode(array("msg" => "<font color=\"green\">Brand name added successfully</font>", "id" => 0));
            } else {
                echo json_encode(array("msg" => "<font color=\"red\">Brand name already used</font>", "id" => 1));
            }
        } else {
            echo json_encode(array("msg" => "<font color=\"red\">Fill all fields properly</font>", "id" => 1));
        }
    }

    function edit($id) {
        if (!empty($id)) {
            $data['brand'] = $this->ib->get_brand_id($id)[0];
            $this->load->view('admin/common/header');
            $this->load->view('admin/operation/item_brand/edit', $data);
            $this->load->view('admin/common/footer');
        }
    }

    function lists() {
        $data['brand'] = $this->ib->get_brand();
        $this->load->view('admin/common/header');
        $this->load->view('admin/operation/item_brand/brand_list', $data);
        $this->load->view('admin/common/footer');
    }

    function edit_item_brand() {
        if (!empty($this->input->post('id_brand')) && !empty($this->input->post('bcode'))) {
            $this->ib->_update($this->input->post('id_brand'), $this->input->post('bcode'));
            echo json_encode(array("id" => $this->input->post('id_brand'), "bcode" => $this->input->post('bcode')));
        }
    }

}

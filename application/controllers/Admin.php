<?php
class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin_model','am');
		$this->load->model('Login_model', 'lm');
    }

    function index() {
        is_login();
		$this->load->view('admin/common/header');
        $this->load->view('admin/admin');
        $this->load->view('admin/common/footer');
    }
	
	function login(){
		$this->load->view('admin/login');
	}
	
	 function validate_login() {
        $email = $this->input->post('signin_email_id');
        $password = md5($this->input->post('signin_password'));
        $count = $this->lm->check_login($email, $password);
        if ($count == 1) {
            $admin = $this->lm->get_admin($email);
            $this->lm->update_last_login($email);
            $this->session->set_userdata($admin);
        }
        echo $count;
    }
	function logout(){
		$array_items = array('email_id', 'role', 'name', 'last_login', 'id');
        $this->session->unset_userdata($array_items);
        redirect('admin/login', 'refresh');
	}
}

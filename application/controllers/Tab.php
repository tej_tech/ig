<?php

Class Tab extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("tab_model", "tm");
    }

    function index() {
        $data['store'] = $this->tm->get_store();
        $data['tab'] = $this->tm->get_tab();

        $this->load->view('admin/common/header');
        $this->load->view('admin/operation/tab/landing_page', $data);
        $this->load->view('admin/common/footer');
    }

    function get_tab_search() {
        $s_name = $this->input->post('s_name');
        $data['tab'] = $this->tm->get_tab_search($s_name);
        echo $this->load->view('admin/operation/tab/search_result', $data, true);
    }

    function get_tab_id() {
        $id = $this->input->post('id');
        $tab = $this->tm->get_tab_id($id)[0];
        echo json_encode($tab);
    }

    function validate() {
        if (!empty($this->input->post('store')) && !empty($this->input->post('tab_name'))) {
            $data = array(
                "store_id" => $this->input->post('store'),
                "model_code" => $this->input->post('tab_name'),
            );
            $v = $this->tm->count_tab($data);
            if ($v > 0) {
                echo json_encode(array("err" => "0"));
            } else {
                echo json_encode(array("err" => "1"));
            }
        } else {
            echo json_encode(array("err" => "2"));
        }
    }

    function save_item_tab() {
        if (!empty($this->input->post('store')) && !empty($this->input->post('tab_name')) && !empty($this->input->post('date_picker')) && !empty($this->input->post('price'))) {
            $data = array(
                "store_id" => $this->input->post('store'),
                "model_code" => $this->input->post('tab_name'),
                "purchase_date" => $this->input->post('date_picker'),
                "price" => $this->input->post('price'),
                "created_by" => $this->session->userdata('id'),
                "updated_by" => $this->session->userdata('id'),
                "updated_date" => date("Y-m-d H:s:i", time())
            );
            $v = $this->tm->save_tab_attr($data);
            echo json_encode(array("err" => 0));
        } else {
            echo json_encode(array("err" => 1));
        }
    }

    function edit_item_tab() {
        if (!empty($this->input->post('store')) && !empty($this->input->post('tab_name')) && !empty($this->input->post('date_picker')) && !empty($this->input->post('price'))) {
            $id = $this->input->post('id');
            $data = array(
                "store_id" => $this->input->post('store'),
                "model_code" => $this->input->post('tab_name'),
                "purchase_date" => $this->input->post('date_picker'),
                "price" => $this->input->post('price'),
                "updated_by" => $this->session->userdata('id'),
                "updated_date" => date("Y-m-d H:s:i", time())
            );
            $v = $this->tm->_update($id, $data);
            echo json_encode(array("err" => 0));
        } else {
            echo json_encode(array("err" => 1));
        }
    }

    function delete() {
     echo   $id = $this->input->post("id");
        $this->tm->delete_update($id);
    }

}

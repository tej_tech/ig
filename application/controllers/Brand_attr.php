<?php

class Brand_attr extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("brand_attr_model", "ba");
    }

    function index() {
        $data['brand_attr'] = $this->ba->get_brand_attr();
        $this->load->view('admin/common/header');
        $this->load->view('admin/operation/brand_attr/landing_page', $data);
        $this->load->view('admin/common/footer');
    }

    function validate_brand_attr_name() {
        if (!empty($this->input->post('aname'))) {
            $c = $this->ba->validate_brand_name($this->input->post('aname'));
            if ($c == 0) {
                echo 0;
            } else {
                echo 1;
            }
        }
    }

    function save_item_brand_attr() {
        if (!empty($this->input->post('type') && $this->input->post('type') == "new")) {
            if (!empty($this->input->post('aname'))) {
                $c = $this->ba->validate_brand_name($this->input->post('aname'));
                $brand_array = array(
                    "attr_name" => $this->input->post('aname'),
                    "attr_desc" => $this->input->post('adesc'),
                    "created_by" => $this->session->userdata('id'),
                    "updated_by" => $this->session->userdata('id'),
                    "updated_date" => date("Y-m-d H:s:i", time())
                );
                if ($c == 0) {
                    $d = $this->ba->save_brand_attr($brand_array);
                    $g = "<tr id=\"brandremove_$d\">
                <td id=\"en_$d\">" . $this->input->post('aname') . "</td>
                <td id=\"ec_$d\">" . $this->input->post('adesc') . "</td>
                <td>
                
                <a href = \"#\" title = \"Edit\"  id=\"upbr_$d\" class = \"btn btn-xs btn-outline update_brand btn-success add-tooltip\"><i class = \"fa fa-pencil\"></i></a>
                <a href = \"#\" title = \"Delete\" id=\"bdel_$d\" class = \"btn btn-xs brand_delete btn-outline btn-danger add-tooltip\"><i class = \"fa fa-times\"></i></a>
                </td>
                </tr>";
                    echo json_encode(array("brand" => $g, "msg" => "<font color=\"green\">Brand name added successfully</font>", "id" => 0));
                } else {
                    echo json_encode(array("msg" => "<font color=\"red\">Brand name already used</font>", "id" => 1));
                }
            } else {
                echo json_encode(array("msg" => "<font color=\"red\">Fill all fields properly</font>", "id" => 1));
            }
        } else {
            echo json_encode(array("msg" => "<font color=\"red\">Something went wrong please refresh the page</font>", "id" => 1));
        }
    }

    function get_brandattr_id() {
        if (!empty($this->input->post('id'))) {
            $c = $this->ba->get_brand_id($this->input->post('id'));
            echo json_encode(array("id" => $c[0]['id'], "baname" => $c[0]['attr_name'], "adesc" => $c[0]['attr_desc']));
        }
    }

    function update_brandattr() {
        if (!empty($this->input->post('type')) && $this->input->post('type') == "edit") {
            if (!empty($this->input->post('id')) && !empty($this->input->post('adesc'))) {
                $this->ba->_update($this->input->post('id'), $this->input->post('adesc'));
                echo json_encode(array("err" => 0, "msg" => "<font color=\"green\">Updated Successfully<font>", "id" => $this->input->post('id'), "adesc" => $this->input->post('adesc')));
            }
        } else {
            echo json_encode(array("err" => 1, "msg" => "<font color=\"red\">Something went wrong please refresh the page</font>", "id" => $this->input->post('id')));
        }
    }

    function delete_brandattr() {
        if (!empty($this->input->post('id'))) {
            $this->ba->delete_update($this->input->post('id'));
            echo $this->input->post('id');
        }
    }

    function get_brand_attr_search() {
        $s_name = $this->input->post('s_name');
        $data['array'] = $this->ba->get_fruits_attr($s_name);
      //  print_r($data['array']);
      //  echo $this->db->last_query();
        echo $this->load->view('admin/operation/brand_attr/search_result', $data,true);
    }

}

<?php

Class Fruits extends CI_Controller {

    function __construct() {
        parent::__construct();
		$id = $this->session->userdata('id');
        if (empty($id)) {
            redirect('/admin_login', 'refresh');
			exit;
        }
        $this->load->model('fruits_model', 'ib');
    }

    function index() {
		$data['msg'] = '';
        $this->load->view('admin/common/header');
        $this->load->view('admin/operation/fruits/landing_page', $data);
        $this->load->view('admin/common/footer');
    }

    function getFruitList() {
        //echo 'sda'.$this->input->post('searchStr');
        if ($this->input->post('searchStr'))
            $searchStr = $this->input->post('searchStr');
        else
            $searchStr = '';
        $data['fruits'] = $this->ib->get_fruits($searchStr);
        $this->load->view('admin/operation/fruits/fruitList', $data);
    }

    function validate_fruit_name() {

        if (!empty($this->input->post('fname'))) {
            $c = $this->ib->validate_fruit_name($this->input->post('fname'));
            if ($c == 0) {
                echo 0;
            } else {
                echo 1;
            }
        }
    }

    function save_fruits() {
        if (!empty($this->input->post('fname'))) {
            $c = $this->ib->validate_fruit_name($this->input->post('fname'));
            $brand_array = array("name" => $this->input->post('fname'));
            if ($c == 0) {
                $d = $this->ib->save_fruit($brand_array);
                /* $g = "<tr id=\"brandremove_$d\">
                  <td id=\"en_$d\">". $this->input->post('fname') . "</td>
                  <td>

                  <a href = \"#\" title = \"Edit\"  data-toggle=\"modal\" data-target=\"#myModal1\"  id=\"upbr_$d\" class = \"btn btn-xs btn-outline update_brand btn-success add-tooltip\"><i class = \"fa fa-pencil\"></i></a>
                  <a href = \"#\" title = \"Delete\" id=\"bdel_$d\" class = \"btn btn-xs brand_delete btn-outline btn-danger add-tooltip\"><i class = \"fa fa-times\"></i></a>
                  </td>
                  </tr>"; */
                echo json_encode(array("msg" => "<font color=\"green\">Fruit name added successfully</font>", "id" => 0));
            } else {
                echo json_encode(array("msg" => "<font color=\"red\">Fruit name already used</font>", "id" => 1));
            }
        } else {
            echo json_encode(array("msg" => "<font color=\"red\">Fill all fields properly</font>", "id" => 1));
        }
    }

    function get_fruit_id() {
        if (!empty($this->input->post('id'))) {
            $c = $this->ib->get_fruit_id($this->input->post('id'));
            echo json_encode(array("id" => $c[0]['id'], "name" => $c[0]['name']));
        }
    }

    function update_fruit() {
        if (!empty($this->input->post('fruit_id')) && !empty($this->input->post('upfruit_name'))) {
            $f = $this->ib->validate_fruit_update($this->input->post('fruit_id'), $this->input->post('upfruit_name'));
            if ($f == 0) {
                $this->ib->_update($this->input->post('fruit_id'), $this->input->post('upfruit_name'));
                echo json_encode(array("status" => 0, "id" => $this->input->post('fruit_id'), "upfruit_name" => $this->input->post('upfruit_name')));
            } else {
                echo json_encode(array("status" => 1));
            }
        }
    }

    function delete_brand() {
        if (!empty($this->input->post('id'))) {
            $this->ib->delete_update($this->input->post('id'));
            echo $this->input->post('id');
        }
    }

}

<?php
class User extends CI_Controller {
    function __construct() {
        parent::__construct();
        is_login();
        if ($this->session->userdata('role') != "admin") {
            redirect('/admin', 'refresh');
        }
        $this->load->model('common_model', 'common');
		$this->load->model('user_model', 'user');
    }

    function index() {
        $this->create();
    }
	function create() {
		$data['msg']='';
		$data['role']='';
		$this->load->helper(array('form','security'));
		$this->load->library('form_validation');
		if($this->input->post('submit')){
			$data['role']=$this->input->post('role');
			$this->form_validation->set_rules('role', 'Role', 'required');
			$this->form_validation->set_rules('name', 'Fullname', 'required');
			$this->form_validation->set_rules(
					'email_id', 'Email',
					'required|min_length[6]|max_length[100]|is_unique[admin_login.email_id]',
					array(
							'required'      => 'You have not provided %s.',
							'is_unique'     => 'This %s already exists.'
					)
			);

			if($this->input->post('role')=='sales'){
				$this->form_validation->set_rules('pin', 'Pin', 'required|alpha_numeric|is_unique[admin_detail.pin]',
					array(
							'is_unique'     => 'This Pin already exists.'
					));
			}
			else{
				$this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric',array('required' => 'You must provide a %s.'));
				$this->form_validation->set_rules('cpassword', 'Password Confirmation', 'required|matches[password]');	
			}
			if($this->input->post('role')!='admin'){
				$this->form_validation->set_rules('mobile', 'Mobile', 'required|numeric');
				$this->form_validation->set_rules('store', 'Store', 'required|numeric');
				$this->form_validation->set_rules('address', 'Address', 'required');
			}
			if($this->form_validation->run()==FALSE){
				$data['store'] = $this->common->get_store();
            	$data['err_msg']='Please try again';
			}
			else{
				 
				$admin_ins['name'] = $this->security->xss_clean($this->input->post('name'));
				$admin_ins['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
				if($this->input->post('password'))
				$admin_ins['password'] = md5($this->input->post('password'));
				$admin_ins['role'] = $this->input->post('role');
				$admin_insdetails['admin_id'] = $this->user->save($admin_ins);
				
				if($admin_ins['role']!='admin'){
					$admin_insdetails['mobile'] = $this->security->xss_clean($this->input->post('mobile'));
					$admin_insdetails['store'] = $this->security->xss_clean($this->input->post('store'));
					$admin_insdetails['address'] = $this->security->xss_clean($this->input->post('address'));	
					if($this->input->post('pin'))
						$admin_insdetails['pin'] = $this->input->post('pin');
					$this->user->save_detail($admin_insdetails);		
				}
				$emailmsg = "Your account has been created on IG International\nLogin details are";
				if($admin_ins['role']!='sales'){
					$emailmsg .= "\nEmail Address: ".$admin_ins['email_id'];
					$emailmsg .= "\nPassword: ".$this->input->post('password');
				}
				else
					$emailmsg .= "\nPin: ".$admin_insdetails['pin'];
				$this->load->library('email');
				$this->email->from('admin@ig-international.com', 'IG International'); 
				$this->email->to($admin_ins['email_id']);
				$this->email->cc('bhavik.bagul@carrottech.in');
				$this->email->subject('IG International : New Account Registration'); 
				$this->email->message($emailmsg); 
				$this->email->send();
				
				redirect('user/lists');
				exit;
			}
		}
        $this->load->view('admin/common/header');
        $this->load->view('admin/admin_register/create_user',$data);
        $this->load->view('admin/common/footer');
    }
	
	function edit($edit=0) {
		$data['edit_user'] = $edit;
		$user_id = substr($edit,4);
		$data['msg']='';
		$data['role']='';
		$this->load->helper(array('form','security'));
		$this->load->library('form_validation');
		$this->load->library('my_form_validation');
		if($this->input->post('submit')){
			$data['role']=$this->input->post('role');
			$this->form_validation->set_rules('name', 'Fullname', 'required');
			
			if($this->input->post('role')=='sales'){
				$this->form_validation->set_rules('pin', 'Pin', 'required|alpha_numeric|is_unique[admin_detail.pin.admin_id.'.$user_id.']');
			}
			else{
				$this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric',array('required' => 'You must provide a %s.'));
				$this->form_validation->set_rules('cpassword', 'Password Confirmation', 'required|matches[password]');	
			}
			if($this->input->post('role')!='admin'){
				$this->form_validation->set_rules('mobile', 'Mobile', 'required|numeric');
				$this->form_validation->set_rules('store', 'Store', 'required|numeric');
				$this->form_validation->set_rules('address', 'Address', 'required');
			}
			if($this->form_validation->run()==FALSE){
				$data['store'] = $this->common->get_store();
            	$data['err_msg']='Please try again';
			}
			else{
				$admin_ins['name'] = $this->security->xss_clean($this->input->post('name'));
				if($this->input->post('password'))
				$admin_ins['password'] = md5($this->input->post('password'));
				$this->user->update_user($admin_ins,$user_id);
				
				if($data['role']!='admin'){
					$admin_insdetails['mobile'] = $this->security->xss_clean($this->input->post('mobile'));
					$admin_insdetails['store'] = $this->security->xss_clean($this->input->post('store'));
					$admin_insdetails['address'] = $this->security->xss_clean($this->input->post('address'));	
					if($this->input->post('pin'))
						$admin_insdetails['pin'] = $this->input->post('pin');
					$this->user->update_user_detail($admin_insdetails,$user_id);		
				}
								
				redirect('user/lists');
				exit;
			}
		}
		$user_detail = $this->user->getUser($user_id);
		if(sizeof($user_detail)==0){
			redirect('user/lists');		
		}
		else
			$data['user'] = $user_detail[0];
		$data['store'] = $this->common->get_store();
        $this->load->view('admin/common/header');
        $this->load->view('admin/admin_register/edit_user',$data);
        $this->load->view('admin/common/footer');
    }
	
	function loadRole(){
		$this->load->helper('form');
		//$this->load->library('form_validation');
		if($this->input->post('role')){
			$data['role'] = $this->input->post('role');
			$data['store'] = $this->common->get_store();
			$this->load->view('admin/admin_register/user_role',$data);
		}
	}
    function lists() {
		$this->load->view('admin/common/header');
        $this->load->view('admin/admin_register/user_list');
        $this->load->view('admin/common/footer');
    }	
	function getUserList() {
		$this->load->helper('string');
		if($this->input->post('roles'))
		$roles = $this->input->post('roles');
		else
		$roles='';
		$data['users'] = $this->user->getUsers($roles);
        $this->load->view('admin/admin_register/users',$data);
    }
}
<?php
class Mapping extends CI_Controller {

    function __construct() {
        parent::__construct();
		is_login();
        $this->load->model('Mapping_model', 'mapping');
		$this->load->model('Common_model', 'common');
    }

    function index() {
        $data['msg'] = '';
		$this->load->library('form_validation');
		$data['fruits'] = $this->common->get_fruits();
		$data['brands'] = $this->common->get_brand();
		$data['brand_attrs'] = $this->common->get_brand_attr();
        $this->load->view('admin/common/header');
        $this->load->view('admin/operation/brand_attr_map/mapping', $data);
        $this->load->view('admin/common/footer');
    }
	
	function getList(){
		//echo 'sda'.$this->input->post('searchStr');
		if($this->input->post('searchStr'))
		$searchStr = $this->input->post('searchStr');
		else
		$searchStr='';
		$data['mapping'] = $this->common->getMappingList($searchStr);
		//print_r($data['mapping']);
		$this->load->view('admin/operation/brand_attr_map/list', $data);	
	}

    function save_mapping($mapId=0) {
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('fruit_id', 'Fruit', 'required|numeric');
		$this->form_validation->set_rules('brand_id', 'Brand', 'required|numeric');
		$this->form_validation->set_rules('attr_id', 'Attribute', 'required|numeric');
		$this->form_validation->set_rules('unit_price_s', 'Unit Price', 'required|numeric');
		$this->form_validation->set_rules('unit_price1', 'Price', 'required|numeric');
		$this->form_validation->set_rules('unit_count', 'Unit Count', 'required|numeric');
		if ($this->form_validation->run() == FALSE){
			echo json_encode(array("msg" => "<font color=\"red\">Fill all fields properly</font>", "id" => 1));
		}
		else{
			if($this->input->post('fruit_id') && $this->input->post('brand_id') && $this->input->post('attr_id') && $this->input->post('unit_price1') && $this->input->post('unit_count')){
				$mappingArr['fruit_id'] = $this->input->post('fruit_id');
				$mappingArr['brand_id'] = $this->input->post('brand_id');
				$mappingArr['attr_id'] = $this->input->post('attr_id');
				$mappingArr['unit_price_s'] = $this->input->post('unit_price_s');
				$mappingArr['unit_price1'] = $this->input->post('unit_price1');
				if($mappingArr['unit_price_s']==1)
				$mappingArr['unit_price2'] = $this->input->post('unit_price2');
				$mappingArr['unit_count'] = $this->input->post('unit_count');
				if($this->input->post('subtype')=='edit'){
					$qry = $this->mapping->edit_mapping($mappingArr,$mapId);
					$msg = 'Mapping updated successfully';
				}
				else{
					$qry = $this->mapping->save_mapping($mappingArr);
					$msg = 'Mapping added successfully';
				}
				if( !$qry)
				{
				   //$errNo   = $this->db->_error_number();
				   //$errMess = $this->db->_error_message();
				   echo json_encode(array("status"=>'error',"msg" => "<font color=\"red\">Something is wrong, please try again.</font>", "id" => 1));
				}
				else
				echo json_encode(array("status"=>'success',"msg" => "<font color=\"green\">$msg</font>", "id" => 0));
			}	
		}
    }

    function edit() {
		$map_id = substr($this->input->post('editId'),5);
		$map_list = $this->mapping->get_mapping($map_id);
		echo json_encode(array("status"=>"success","result" => $map_list));
    }

    function delete() {
        if (!empty($this->input->post('deleteId'))) {
			$map_id = substr($this->input->post('deleteId'),5);
           $qry =  $this->mapping->delete_update($map_id);
            if( !$qry){
				   echo json_encode(array("status"=>"error","msg" => "<font color=\"red\">Something is wrong, please try again.</font>", "id" => 1));
			}
			else
				echo json_encode(array("status"=>"success","msg" => "<font color=\"green\">Record deleted succesfully</font>", "id" => 0));
        }
    }

}

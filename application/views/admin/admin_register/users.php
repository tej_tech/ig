<?php
if(sizeof($users)>0){
	$cnt=1;
	foreach ($users as $b) { ?>
        <tr id="brandremove_<?php echo $b['id']; ?>">
        	<td><?=$cnt?></td>
            <td><?php echo $b['name']; ?></td>
            <td><?php echo $b['email_id']; ?></td>
            <td><?php echo $b['role']; ?></td>
            <td><?php if($b['store']!=null) echo $this->common->get_storeName($b['store']); ?></td>
            <td><?php echo $b['mobile']; ?></td>
            <td><?php echo $b['address']; ?></td><td><?php echo $b['pin']; ?></td>
            <td>
                <a href="<?=site_url('user/edit/'.random_string('numeric', 4).$b['id'])?>" title="Edit" class="btn btn-xs btn-outline update_brand btn-success add-tooltip"><i class="fa fa-pencil"></i></a>
                <!--<a href="#" title="Delete" class="btn brand_delete btn-xs btn-outline btn-danger add-tooltip"><i class="fa fa-times"></i></a>-->
            </td>
        </tr>
    <?php $cnt++;
	}
}
else{
	echo '<tr><td colspan="8">There are not any matching records</td></tr>';	
}
?>
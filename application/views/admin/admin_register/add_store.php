<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">Home</a></li>
    <li><a href="#">Register</a></li>
    <li class="active"><a href="#">Add Store</a></li>
</ul>
<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Add Store</span>
            </div>
            <div class="panel-body">
                <form class="form-horizontal"  id="add_store_form" method="post">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Store name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="store_name" id="store_name" class="form-control" placeholder="Full name">
                                    <span id="sts_err"></span>
                                </div>
                            </div> 
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="email_s" class="col-sm-3 control-label">Email-id</label>
                                <div class="col-sm-9">
                                    <input type="text" name="email_s" id="email_s" class="form-control" placeholder="E-mail">
                                    <span id="emails_err"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="mob_s" class="col-sm-3 control-label">Mobile</label>
                                <div class="col-sm-9">
                                    <input type="text" name="mob_s" id="mob_s" class="form-control" placeholder="Mobile number">
                                    <span id="mobs_err"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="address_s" class="col-sm-3 control-label">Street address</label>
                                <div class="col-sm-9">
                                    <textarea name="address_s" id="address_s" class="form-control" placeholder="Street address"></textarea>
                                    <span id="streets_err"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="state_s" class="col-sm-3 control-label">State</label>
                                <div class="col-sm-9">
                                    <select name="state_s" id="state_s" class="form-control form-group-margin">
                                        <?php foreach ($state as $s) { ?>
                                            <option value="<?php echo $s['state_id']; ?>"><?php echo $s['state_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span id="states_err"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="cd_s" class="col-sm-3 control-label">City/District</label>
                                <div class="col-sm-9">
                                    <input type="hidden" id="state_rep" name="state_rep" />
                                    <select name="cd_s" id="cd_s" class="form-control form-group-margin">
                                        <?php foreach ($cd as $c) { ?>
                                            <option value="<?php echo $c['location_name']; ?>"><?php echo $c['location_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span id="cds_err"></span>
                                </div>
                            </div>
                        </div>
                    </div>  

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="pin_s" class="col-sm-3 control-label">Pin code</label>
                                <div class="col-sm-9">
                                    <input type="text" name="pin_s" id="pin_s" class="form-control" placeholder="Pin code">
                                    <span id="pins_err"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <input type="submit" id="adds_btn" value="Create an account" class="btn btn-primary btn-lg btn-block">
                                    <span id="alls_err"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function () {
	$("#cd_s").select2({placeholder: "Select District",allowClear: true});
	$("#state_s").change(function () {
		var id = $(this).val();
		var t = $("#state_s :selected").text();
		$("#state_rep").val(t);
		$.ajax({
			type: 'post',
			url: '<?php echo base_url() . "add_store/get_location"; ?>',
			data: 'id=' + id,
			success: function (res) {
				$("#cd_s").html(res);
				$("#cd_s").select2();
			}
		});

	});

	$("#store_name").keyup(check_sname);
	$("#store_name").blur(check_sname);
	$("#email_s").keyup(check_semail);
	$("#email_s").blur(check_semail);
	$("#address_s").keyup(check_saddress);
	$("#address_s").blur(check_saddress);
	$("#mob_s").keyup(check_mob_s);
	$("#mob_s").blur(check_mob_s);
	$("#pin_s").keyup(check_pin_s);
	$("#pin_s").blur(check_pin_s);
	$("#mob_s").keydown(function (event) {
		// Allow only backspace and delete
		if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
			// let it happen, don't do anything
		} else {
			if (event.keyCode < 48 || event.keyCode > 57) {
				if ($("#mob_s").val().length == 10) {
					$("#mobs_err").html('');
				} else {
					$("#mobs_err").html('<font color="red">Only digits are allowed</font>');
				}
				event.preventDefault();
			}
		}
	});

	function check_mob_s() {
		if ($("#mob_s").val().length == 0) {
			$("#mobs_err").parent().addClass('has-error');
			$("#mobs_err").html('<font color="red">Required field</font>');
			return false;
		} else if ($("#mob_s").val().length != 10) {
			$("#mobs_err").parent().addClass('has-error');
			$("#mobs_err").html('<font color="red">Phone number should be 10 number</font>');
			return false;
		} else if (!$.isNumeric($("#mob_s").val())) {
			$("#mobs_err").html('<font color="red">Only numbers are allowed</font>');
			return false;
		} else {
			$("#mobs_err").parent().removeClass('has-error');
			$("#mobs_err").html('<font color="green">Done</font>');
			return true;
		}
	}

	$("#pin_s").keydown(function (event) {
		// Allow only backspace and delete
		if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9) {
			// let it happen, don't do anything
		} else {
			if (event.keyCode < 48 || event.keyCode > 57) {
				if ($("#pin_s").val().length == 10) {
					$("#pins_err").html('');
				} else {
					$("#pins_err").html('<font color="red">Only digits are allowed</font>');
				}
				event.preventDefault();
			}
		}
	});

	function check_pin_s() {
		if ($("#pin_s").val().length == 0) {
			$("#pins_err").parent().addClass('has-error');
			$("#pins_err").html('<font color="red">Required field</font>');
			return false;
		} else if (!$.isNumeric($("#pin_s").val())) {
			$("#pins_err").html('<font color="red">Only numbers are allowed</font>');
			return false;
		} else {
			$("#pins_err").parent().removeClass('has-error');
			$("#pins_err").html('<font color="green">Done</font>');
			return true;
		}
	}


	function check_sname() {
		if ($("#store_name").val().length == 0) {
			$("#sts_err").parent().addClass('has-error');
			$("#sts_err").html('<font color="red">Required field</font>');
			return false;
		} else {
			$("#sts_err").parent().removeClass('has-error');
			$("#sts_err").html('<font color="green">Done</font>');
			return true;
		}
	}

	function check_semail() {
		var email = $("#email_s").val();

		var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
		if (email.length == 0) {
			$("#emails_err").parent().addClass('has-error');
			$("#emails_err").html('<font color="red">Required field</font>');
			return false;
		} else if (!filter.test(email)) {
			$("#emails_err").parent().addClass('has-error');
			$("#emails_err").html('<font color="red">Invaid email id</font>');
			return false;
		} else {
			$("#emails_err").parent().removeClass('has-error');
			$("#emails_err").html('');
			return true;
		}
	}

	function check_saddress() {
		if ($("#address_s").val().length == 0) {
			$("#streets_err").parent().addClass('has-error');
			$("#streets_err").html('<font color="red">Required field</font>');
			return false;
		} else {
			$("#streets_err").parent().removeClass('has-error');
			$("#streets_err").html('<font color="green">Done</font>');
			return true;
		}
	}
	$("#add_store_form").on('submit', function (e) {
		if (check_sname() & check_semail() & check_saddress() & check_mob_s() & check_pin_s()) {
			$("#adds_btn").attr("disabled", "disabled")
			$("#alls_err").html('<font color="blue">Please wait...</font>');
			$.ajax({
				type: 'post',
				url: '<?php echo base_url() . "add_store/validate_add_store"; ?>',
				data: new FormData(this),
				processData: false,
				contentType: false,
				success: function (res) {
					$("#adds_btn").removeAttr("disabled")
					if (res == "0") {
						$("#alls_err").html('<font color="red">Fill all fields properly</font>');
					} else if (res == "1") {
						$("#alls_err").html('<font color="red">Store name for this location has been already taken</font>');
					} else {
						//  document.getElementById("add_store_form").reset();
						$("#add_store_form span").html('');
						$("#alls_err").html('<font color="green">Store Added successfully</font>');
					}
				}
			});
		} else {
			$("#alls_err").html('<font color="red">Fill all fields properly</font>');
		}
		e.preventDefault();
		return false;
	});
});
</script>
<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">User Setting</a></li>
    <li class="active"><a href="#">Users List</a></li>
</ul>


<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Users List</span>
            </div>
            <div class="panel-body">
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="col-xs-8">
                        <select name="roles" id="roles" class="form-control">
                        	<option value="">All</option>
                            <option value="admin">Admin</option>
                            <option value="sales">Sales Executive</option>
                            <option value="cashier">Cashier</option>
                        </select>                                
                        </div>
                        <!--<div class="col-xs-4">
                                <input type="button" id="search_btn" value="Search" class="btn btn-primary btn-block">
                        </div>-->	
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <div class="col-xs-9"></div>
                        <div class="col-xs-3"><a href="<?=site_url('user/create')?>" class="btn btn-primary btn-labeled" style="width: 100%;"><span class="btn-label icon fa fa-plus"></span>Add New User</a></div>	
                    </div>
                </div>
                
            </div>                
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">List of Users</span>
        </div>
    	<div class="panel-body">
        <div class="table-primary">
        <table class="table table-bordered" id="userList" width="100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th><th>Email ID</th><th>Role</th><th>Store</th><th>Mobile</th><th>Address</th><th>Pin</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            
        </tbody>
    </table></div>
        </div>
    </div>
    </div>
    
</div>

<script>
$(document).ready(function () {
	$("#roles").on('change', function (e) {
		var roles = $("#roles").val();
		$.ajax({
				type: 'post',
				url: '<?php echo base_url() . "user/getUserList"; ?>',
				data: {roles:roles},
				dataType: "html",
				success: function (res) {
					$("#userList tbody").html(res);
				}
			});
	});
	$("#roles").trigger('change');	
});
</script>
<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">User Setting</a></li>
    <li class="active"><a href="#">Edit User</a></li>
</ul>

<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Edit User</span>
            </div>
            <div class="panel-body">
            
        <div class="row col-xs-12 has-error" id="errorDiv"><div class="control-label"><?
		$role = $user['role'];
		if(isset($err_msg))
		echo validation_errors();
		else
		echo $msg?></div></div>
        <?php echo form_open('user/edit/'.$edit_user, 'class="form-horizontal" id="frm"');  ?>
                <!--<form class="form-horizontal"  id="signup_form" method="post">-->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Role</label><input type="hidden" name="role" value="<?php echo $user['role'] ?>" />
                        <label class="col-sm-9 control-label text-left"><?php echo $user['role'] ?></label>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Full name</label>
                        <div class="col-sm-5">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Full name" required value="<?php echo stripslashes($user['name']); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Email-id</label>
                        <label class="col-sm-9 control-label text-left"><?php echo $user['email_id'] ?></label>
                    </div>
					<?php if($role=='sales'){ ?>
                    <div class="form-group">
                        <label for="pin" class="col-sm-3 control-label">Pin</label>
                        <div class="col-sm-5">
                            <input type="text" name="pin" id="pin" class="form-control" placeholder="Pin" required value="<?php echo $user['pin'] ?>">
                        </div>
                    </div>
                    <?php } 
                    else{
                    ?>
                    <div class="form-group">
                        <label for="pass" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-5">
                            <input type="password" name="password" id="pass" class="form-control" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pass" class="col-sm-3 control-label">Confirm Password</label>
                        <div class="col-sm-5">
                            <input type="password" name="cpassword" id="cpass" class="form-control" placeholder="Confirm Password" required>
                        </div>
                    </div>
                    <?php }
                    if($role!='admin'){
                    ?>
                    <div class="form-group">
                        <label for="mobile" class="col-sm-3 control-label">Mobile</label>
                        <div class="col-sm-5">
                            <input type="text" name="mobile" id="mobile" class="form-control numberonly" placeholder="Mobile number" required value="<?php echo $user['mobile'] ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="store" class="col-sm-3 control-label">Store Name</label>
                        <div class="col-sm-5">
                            <select name="store" id="store" class="form-control form-group-margin" required>
                                <?php foreach ($store as $t) { ?>
                                    <option value="<?php echo $t['id']; ?>" <? if($user['store']==$t) echo 'selected="selected"' ?>><?php echo $t['store_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-5">
                            <textarea name="address" id="address" class="form-control" placeholder="Street address" required><?php echo stripslashes($user['address']); ?></textarea>
                            <span id="address_err"></span>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-3">
                            <input type="submit" name="submit" value="Save" class="btn btn-primary btn-lg btn-block">
                            <span id="all_err"></span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

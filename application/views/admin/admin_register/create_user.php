<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">User Setting</a></li>
    <li class="active"><a href="#">Add New User</a></li>
</ul>

<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Add New User</span>
            </div>
            <div class="panel-body">
                <div class="row col-xs-12 has-error" id="errorDiv"><div class="control-label"><?
                if(isset($err_msg))
                echo validation_errors();
                else
                echo $msg?></div></div>
                <?php echo form_open('user/create', 'class="form-horizontal" id="frm"');  ?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Role</label>
                        <div class="col-sm-9">
                            <select name="role" id="role" class="form-control" placeholder="Select Role" required>
                            	<option value="">Please select</option>
                                <option value="admin" <? if($this->input->post('role') && $this->input->post('role')=='admin') echo 'selected="selected"' ?>>Admin</option>
                                <option value="cashier" <? if($this->input->post('role') && $this->input->post('role')=='cashier') echo 'selected="selected"' ?>>Cashier</option>
                                <option value="sales" <? if($this->input->post('role') && $this->input->post('role')=='sales') echo 'selected="selected"' ?>>Sales Executive</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Full name</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Full name" required value="<?php echo set_value('name'); ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Email-id</label>
                        <div class="col-sm-9">
                            <input type="text" name="email_id" id="email_id" class="form-control" placeholder="E-mail" required value="<?php echo set_value('email_id'); ?>">
                        </div>
                    </div>
					<div id="user_detail"><?php
                    if($role!=''){
						$this->load->view('admin/admin_register/user_role');
					}
					?></div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {
	$("#role").on('change',function(){
		var role = $(this).val();
		if(role!=''){
			$.ajax({
				type: 'post',
				url: '<?php echo base_url() . "user/loadRole"; ?>',
				data: 'role=' + role,
				success: function (res) {
					//console.log(res);
					$("#user_detail").html(res);
				}
			});
		}
		else{
			$("#user_detail").html('');	
		}
	});
});
</script>
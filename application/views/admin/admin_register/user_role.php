<?php if($role=='sales'){ ?>
<div class="form-group">
    <label for="pin" class="col-sm-3 control-label">Pin</label>
    <div class="col-sm-9">
        <input type="text" name="pin" id="pin" class="form-control" placeholder="Pin" required value="<?php echo set_value('pin'); ?>">
    </div>
</div>
<?php } 
else{
?>
<div class="form-group">
    <label for="pass" class="col-sm-3 control-label">Password</label>
    <div class="col-sm-9">
        <input type="password" name="password" id="pass" class="form-control" placeholder="Password" required>
    </div>
</div>
<div class="form-group">
    <label for="pass" class="col-sm-3 control-label">Confirm Password</label>
    <div class="col-sm-9">
        <input type="password" name="cpassword" id="cpass" class="form-control" placeholder="Password" required>
    </div>
</div>
<?php }
if($role!='admin'){
?>
<div class="form-group">
    <label for="mobile" class="col-sm-3 control-label">Mobile</label>
    <div class="col-sm-9">
        <input type="text" name="mobile" id="mobile" class="form-control numberonly" placeholder="Mobile number" required value="<?php echo set_value('mobile'); ?>">
    </div>
</div>
<div class="form-group">
    <label for="store" class="col-sm-3 control-label">Store Name</label>
    <div class="col-sm-9">
        <select name="store" id="store" class="form-control form-group-margin" required>
            <?php foreach ($store as $t) { ?>
                <option value="<?php echo $t['id']; ?>"><?php echo $t['store_name']; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="address" class="col-sm-3 control-label">Address</label>
    <div class="col-sm-9">
        <textarea name="address" id="address" class="form-control" placeholder="Street address" required><?php echo set_value('address'); ?></textarea>
    </div>
</div>
<?php } ?>
<div class="form-group">
    <div class="col-sm-offset-3 col-sm-3">
        <input type="submit" name="submit" value="Create an account" class="btn btn-primary btn-lg btn-block">
    </div>
</div>
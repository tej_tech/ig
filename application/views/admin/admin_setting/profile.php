
                <div class="profile-full-name">
                    <span class="text-semibold" id="name_head" style="text-transform: capitalize"><?php echo $ad['name']; ?></span>'s profile
                </div>
                <div class="profile-row">
                    <div class="left-col">
                        <div class="profile-block">
                            <form id='image_profile_setting' role="form" name='image_profile_setting' enctype="multipart/form-data">
                                <div class="panel profile-photo">
                                    <img src="<?php echo image_url($ad['image_url'], 'thumb'); ?>" id="profile_image" alt="">
                                </div><br>
                                <div class="form-group">
                                    <label for="se_img" class="control-label">Select image</label>
                                    <input type="file" class="form-control" name="se_img" id="se_img">
                                    <span id="se_img_err"></span>
                                </div>
                                <input type='submit' id='image_profile_setting_btn'  class="btn btn-success" value="Upload Image"/>
                            </form>
                        </div>
                    </div>
                    <div class="right-col">
                        <hr class="profile-content-hr no-grid-gutter-h">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <span class="panel-title">Profile Setting</span>
                                    </div>
                                    <div class="panel-body">
                                        <form class="form-horizontal" id="profile_update_setting" name='profile_update_setting'>
                                            <div class="form-group">
                                                <label for="jq-validation-email" class="col-sm-3 control-label">Email</label>
                                                <div class="col-sm-9">
                                                    <label for="jq-validation-email" ><?php echo $ad['email_id']; ?></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="jq-validation-required" class="col-sm-3 control-label">Name</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="name" value="<?php echo $ad['name']; ?>" name="name" placeholder="Required">
                                                    <span  id="name_err"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="jq-validation-required" class="col-sm-3 control-label"></label>
                                                <div class="col-sm-9">
                                                    <div class="alert alert-info">
                                                        <strong>Info!</strong> Leave password field empty if don't want to set new one
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="pass" class="col-sm-3 control-label">Password</label>
                                                <div class="col-sm-9">
                                                    <input type="password" class="form-control" id="pass" name="pass" placeholder="Password">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="repass" class="col-sm-3 control-label">Confirm password</label>
                                                <div class="col-sm-9">
                                                    <input type="password" class="form-control" id="repass" name="repass" placeholder="Confirm password">
                                                    <span  id="pass_err"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-9">
                                                    <button type="submit" id='setting_update_btn' name="setting_update_btn" class="btn btn-primary">Update</button>
                                                    <span id='all_err_u'></span>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div> <!-- / #content-wrapper -->

            <div id="main-menu-bg"></div>
        </div> <!-- / #main-wrapper -->
        <script>
            $(document).ready(function () {
                $("#name").keyup(check_name);
                $("#name").blur(check_name);
                $("#repass").keyup(check_pass);
                $("#repass").blur(check_pass);


                function check_name() {
                    if ($("#name").val().length == 0) {
                        $("#name_err").parent().addClass('has-error');
                        $("#name_err").html('<font color="red">Required field</font>');
                        return false;
                    } else {
                        $("#name_err").parent().removeClass('has-error');
                        $("#name_err").html('<font color="green">Done</font>');
                        return true;
                    }
                }

                function check_pass() {
                    if ($("#pass").val().length > 0) {
                        if ($("#pass").val() == $("#repass").val()) {
                            $("#pass_err").parent().removeClass('has-error');
                            $("#pass_err").html('<font color="green">Done</font>');
                            return true;
                        } else {
                            $("#pass_err").parent().addClass('has-error');
                            $("#pass_err").html('<font color="red">password doesn\'t match</font>');
                            return false;
                        }
                    } else {
                        return true;
                    }
                }

                $("#profile_update_setting").on('submit', function (e) {
                    if (check_name() & check_pass()) {
                        $("#setting_update_btn").attr("disabled", "disabled")
                        $("#all_err_u").html('<font color="blue">Please wait...</font>');
                        $.ajax({
                            type: 'post',
                            url: '<?php echo base_url() . "admin_profile_setting/update"; ?>',
                            data: new FormData(this),
                            processData: false,
                            contentType: false,
                            success: function (res) {
                                $("#name_head").text($("#name").val());
                                $(".top_name").text($("#name").val());
                                $("#setting_update_btn").removeAttr("disabled")
                                $("#profile_update_setting span").html('');
                                $("#all_err_u").html('<font color="green">Update done</font>');
                            }
                        });
                    } else {
                        $("#all_err_u").html('<font color="red">Fill all fields properly</font>');
                    }
                    e.preventDefault();
                });




                $("#se_img").change(check_img);
                function check_img() {
                    if ($("#se_img").val().length === 0) {
                        $("#se_img_err").parent().addClass('has-error');
                        $("#se_img_err").html('<font color="red">Required field</font>');
                        return false;
                    } else {
                        $("#se_img_err").parent().removeClass('has-error');
                        $("#se_img_err").html('<font color="green">Done</font>');
                        return true;
                    }
                }

                $("#image_profile_setting").on('submit', function (e) {
                    if (check_img()) {
                        $("#image_profile_setting_btn").attr("disabled", "disabled")
                        $("#all_err_img").html('<font color="blue">Please wait...</font>');
                        $.ajax({
                            type: 'post',
                            url: '<?php echo base_url() . "admin_profile_setting/update_image"; ?>',
                            data: new FormData(this),
                            processData: false,
                            contentType: false,
                            success: function (res) {
                                $("#image_profile_setting_btn").removeAttr("disabled")
                                $("#image_profile_setting span").html('');
                                $("#all_err_img").html('<font color="green">Update done</font>');
                                 $(".image_icon_session").attr("src","<?php echo $this->config->item('icon'); ?>"+res );
                                $("#profile_image").attr("src","<?php echo $this->config->item('thumb'); ?>"+res );
                            }
                        });
                    } else {
                        $("#all_err_u").html('<font color="red">Fill all fields properly</font>');
                    }
                    e.preventDefault();
                });
            });
        </script>
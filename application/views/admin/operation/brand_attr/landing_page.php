<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">Home</a></li>
    <li><a href="#">Operation</a></li>
    <li class="active"><a href="#">Item / brands Attribute</a></li>
</ul>

<div>
    <div class="row">
        <div class="col-sm-4">
            <form class="form-horizontal switch_th"  id="add_branda_form" method="post">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">

                            <div class="col-sm-12">
                                <input type="text" name="aname" id="aname" class="form-control input-md" placeholder="Attribute Name" />
                                <span id="aname_err"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea class="form-control" rows="5" placeholder="Description" name="adesc" id="adesc"></textarea>
                                <span id="desc_err"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-12 button_at">
                                <input type='hidden' name='type' id="type" value='new'>
                                <input type="submit" id="brandaf_btn" value="Add brand Attribute" class="btn btn-primary btn-md btn-block">
                                <span id="branda_err"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-8">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">Item / brands Attribute</span> <span class="panel-title" style="float:right;margin-top: -7px;"><input  type="text" placeholder="Search Attribute" id="search_attr" class="form-control"></span>
                </div>
                <div class="panel-body">
                    <!-- Info table -->
                    <div class="table-primary">

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Edit/Delete</th>
                                </tr>
                            </thead>
                            <tbody id="add_brand_tbl">
                                <?php
                                if (is_array($brand_attr)) {
                                    foreach ($brand_attr as $b) {
                                        ?>
                                        <tr class="brand_remove_la" id="brandremove_<?php echo $b['id']; ?>">
                                            <td id="en_<?php echo $b['id']; ?>"><?php echo $b['attr_name']; ?></td>
                                            <td id="ec_<?php echo $b['id']; ?>"><?php echo $b['attr_desc']; ?></td>
                                            <td>
                                                <a href="#" title="Edit" id="upbr_<?php echo $b['id']; ?>" class="btn btn-xs btn-outline update_brand btn-success add-tooltip"><i class="fa fa-pencil"></i></a>
                                                <a href="#" title="Delete" id="bdel_<?php echo $b['id']; ?>" class="btn brand_delete btn-xs btn-outline btn-danger add-tooltip"><i class="fa fa-times"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- / Info table -->
                </div>
            </div>
            <!-- /5. $CONTROLS -->
        </div>
    </div>
</div> <!-- / #content-wrapper -->
<script>
    $(document).ready(function () {

        $("#search_attr").keyup(function () {
           var sdt =  "s_name="+$(this).val();
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "brand_attr/get_brand_attr_search"; ?>',
                data: sdt,
                success: function (resd) {
                    $("#add_brand_tbl").html(resd);
                }
            });
        });

        $("html body").on("click", ".update_brand", function () {
            var fg = $(this).attr("id");
            var f = fg.split("_");
            var sdt = "id=" + f[1];
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "brand_attr/get_brandattr_id"; ?>',
                dataType: "json",
                data: sdt,
                success: function (res) {
                    $("#aname").attr("name", "");
                    $("#aname").attr("disabled", "disabled");
                    $("#aname").val(res.baname);
                    $(".button_at").html("<input type='hidden' name='type' id='type'  value='edit'><input type='hidden' id='id' name='id' value='" + res.id + "'><input type='submit' id='upbranda_btn' value='Update brand Attribute' class='btn btn-primary btn-md btn-block'><span id='branda_err'></span><a href='#' id='change_to_add_attr' value='Add brand Attribute' class='pull-right'>Add new Attribute</a>");
                    $("#adesc").val(res.adesc);
                    $(".up_branderr").html("");
                    $(".up_branderr").attr("id", "upbranderr_" + res.id);
                }
            });
        });

        $("html body").on("click", "#change_to_add_attr", function () {
            document.getElementById("add_branda_form").reset();
            $("#add_branda_form span").html('');
            $("#aname").attr("name", "aname");
            $("#aname").removeAttr("disabled");
            $(".button_at").html("<input type='hidden' name='type' id='type' value='new'><input type='submit' id='branda_btn' value='Add brand Attribute' class='btn btn-primary btn-md btn-block'><span id='branda_err'></span>");
        });

//        $("html body").on("submit", "#update_brand_form", function (e) {
//
//            e.preventDefault();
//            return false;
//        });

        $("#add_brand_tbl").on("click", ".brand_delete", function () {
            if (confirm("Are you sure you want to delete?")) {
                var f = $(this).attr("id");
                var d = f.split("_");
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url() . "brand_attr/delete_brandattr"; ?>',
                    data: 'id=' + d[1],
                    success: function (res) {
                        $("#brandremove_" + res).remove();
                    }
                });
            }
            return false;
        });


        $("#aname").keyup(check_baname);
        $("#aname").blur(check_baname);


        function brand_ajax(brand_name) {
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "brand_attr/validate_brand_attr_name"; ?>',
                data: 'aname=' + brand_name,
                success: function (res) {
                    if (res === "0") {
                        $("#aname_err").parent().removeClass('has-error');
                        $("#aname_err").html('<font color="green">Done</font>');
                        return true;
                    } else if (res === "1") {
                        $("#aname_err").parent().addClass('has-error');
                        $("#aname_err").html('<font color="red">Brand name already used</font>');
                        return false;
                    }
                }
            });
        }

        function check_baname() {
            if ($("#aname").val().length == 0) {
                $("#aname_err").parent().addClass('has-error');
                $("#aname_err").html('<font color="red">Required field</font>');
                return false;
            } else {
                brand_ajax($("#aname").val());
                return true;
            }
        }

        //  $("html body").on("submit", "#add_branda_form", function (e) {

        $("#add_branda_form").on('submit', function (e) {
            if ($("#type").val() === "new") {
                if (check_baname()) {
                    $("#branda_btn").attr("disabled", "disabled")
                    $("#branda_err").html('<font color="blue">Please wait...</font>');
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url() . "brand_attr/save_item_brand_attr"; ?>',
                        data: new FormData(this),
                        dataType: "json",
                        processData: false,
                        contentType: false,
                        success: function (res) {
                            $("#branda_btn").removeAttr("disabled")
                            $("#branda_err").html(res.msg);
                            $("#add_brand_tbl").prepend(res.brand);
                            if (res.id == 0) {
                                //$('#add_brand_tbl tr:last').remove();
                                document.getElementById("add_branda_form").reset();
                                $("#add_branda_form span").html('');
                            }
                        }
                    });
                } else {
                    $("#brand_err").html('<font color="red">Fill all fields properly</font>');
                }
            } else if ($("#type").val() === "edit") {
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url() . "brand_attr/update_brandattr"; ?>',
                    data: new FormData(this),
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    success: function (res) {
                        if (res.err === 0) {
                            $("#ec_" + res.id).text(res.adesc);
                            $("#branda_err").html(res.msg);
                        } else {
                            $("#branda_err").html(res.msg);
                        }

                    }
                });
            }

            e.preventDefault();
            return false;
        });
    });
</script>
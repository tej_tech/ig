<?php foreach ($array as $v) { ?>
    <tr class="brand_remove_la" id="brandremove_<?php echo $v['id'] ?>">
        <td id="en_<?php echo $v['id'] ?>"><?php echo $v['attr_name'] ?></td>
        <td id="ec_<?php echo $v['id'] ?>"><?php echo $v['attr_desc'] ?></td>
        <td>
            <a href="#" title="Edit" id="upbr_<?php echo $v['id'] ?>" class="btn btn-xs btn-outline update_brand btn-success add-tooltip"><i class="fa fa-pencil"></i></a>
            <a href="#" title="Delete" id="bdel_<?php echo $v['id'] ?>" class="btn brand_delete btn-xs btn-outline btn-danger add-tooltip"><i class="fa fa-times"></i></a>
        </td>
    </tr>
<?php } ?>

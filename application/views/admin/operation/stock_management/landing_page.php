<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">Home</a></li>
    <li><a href="#">Operation</a></li>
    <li class="active"><a href="#">Stock management</a></li>
</ul>
<div>
    <div class="row">
        <div class="col-sm-4">
            <form class="form-horizontal switch_th"  id="add_branda_form" method="post">
                <div class="row">
                    <div class="col-xs-12">
                        <select name="map_id" id="map_id" class="form-control form-group-margin">
                            <?php foreach ($drop as $k => $t) { ?>
                                <optgroup label="<?php echo $k; ?>">
                                    <?php
                                    foreach ($t as $f) {
                                        ?> 
                                        <option value="<?php echo $f['id']; ?>"><?php echo $f['attr_name'] . ' - ' . $f['b_name']; ?></option>
                                    <?php } ?>
                                </optgroup>
                            <?php } ?>
                        </select>
                        <span id="store_err"></span>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="number" name="quantity"  min="1" id="quantity" required class="form-control input-md" placeholder="Quantity">
                                <span id="price_err"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-12 button_at">
                                <input type='hidden' name='type' id="type" value='new'>
                                <input type="submit" id="tab_btn" value="Add stock" class="btn btn-primary btn-md btn-block">
                                <span id="alltab_err"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-8">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">Stock management</span> <span class="panel-title" style="float:right;margin-top: -7px;"><input  type="text" id="search_attr" placeholder="Search Attribute" class="form-control"></span>
                </div>
                <div class="panel-body">
                    <!-- Info table -->
                    <div class="table-primary">

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Fruit</th>
                                    <th>Attribute</th>
                                    <th>Brand</th>
                                    <th>Quantity</th>
                                    <th>Edit/Delete</th>
                                </tr> 
                            </thead>
                            <tbody id="add_brand_tbl">
                                <?php
                                if (is_array($list)) {
                                    foreach ($list as $b) {
                                        ?>
                                        <tr class="brand_remove_la" id="brandremove_<?php echo $b['map_id']; ?>">
                                            <td id="date_<?php echo $b['map_id']; ?>"><?php echo $b['fruit_name']; ?></td>
                                            <td id="date_<?php echo $b['map_id']; ?>"><?php echo $b['attr_name']; ?></td>
                                            <td id="date_<?php echo $b['map_id']; ?>"><?php echo $b['brand_name']; ?></td>
                                            <td id="str_<?php echo $b['map_id']; ?>"><?php echo $b['quantity']; ?></td>
                                            <td>
                                                <a href="#" title="Edit" id="upbr_<?php echo $b['map_id']; ?>" class="btn btn-xs btn-outline update_brand btn-success add-tooltip"><i class="fa fa-pencil"></i></a>
                                                <a href="#" title="Delete" id="bdel_<?php echo $b['map_id']; ?>" class="btn brand_delete btn-xs btn-outline btn-danger add-tooltip"><i class="fa fa-times"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- / Info table -->
                </div>
            </div>
            <!-- /5. $CONTROLS -->
        </div>
    </div>
</div> <!-- / #content-wrapper -->
<script>
    $(document).ready(function () {

        $("#search_attr").keyup(function () {
            var sdt = "s_name=" + $(this).val();
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "tab/get_tab_search"; ?>',
                data: sdt,
                success: function (resd) {
                    $("#add_brand_tbl").html(resd);
                }
            });
        });


        $("html body").on("click", ".update_brand", function () {
            var fg = $(this).attr("id");
            var f = fg.split("_");
            var sdt = "id=" + f[1];
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "stock_management/get_by_id"; ?>',
                dataType: "json",
                data: sdt,
                success: function (res) {
                    $("#quantity").val(res.quantity);
                    $("#map_id").val(res.map_id).change();
                    $(".button_at").html("<input type='hidden' name='type' id='type'  value='edit'><input type='hidden' id='id' name='id' value='" + res.map_id + "'><input type='submit' id='upbranda_btn' value='Update stock' class='btn btn-primary btn-md btn-block'><span id='branda_err'></span><!--<a href='#' id='change_to_add_attr' value='Add brand Attribute' class='pull-right'>Add new Attribute</a>-->");
                }
            });
        });

        $("html body").on("click", "#change_to_add_attr", function () {
            document.getElementById("add_branda_form").reset();
            //  $("#store").html('<option></option><?php // foreach ($store as $t) {      ?><option value="<?php //echo $t['id'];      ?>"><?php //echo $t['store_name'];      ?></option><?php //}      ?>');
            $("#add_branda_form span").html('');
            $("#aname").attr("name", "aname");
            $("#aname").removeAttr("disabled");
            $(".button_at").html("<input type='hidden' name='type' id='type' value='new'><input type='submit' id='branda_btn' value='Add Stock' class='btn btn-primary btn-md btn-block'><span id='branda_err'></span>");
        });

        $("#add_brand_tbl").on("click", ".brand_delete", function () {
            if (confirm("Are you sure you want to delete?")) {
                var f = $(this).attr("id");
                var d = f.split("_");
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url() . "stock_management/delete"; ?>',
                    data: 'id=' + d[1],
                    success: function (res) {
                        $("#brandremove_" + res).remove();
                    }
                });
            }
            return false;
        });

        $("#add_branda_form").on('submit', function (e) {
            if ($("#type").val() === "new") {
                $("#tab_btn").attr("disabled", "disabled")
                $("#alltab_err").html('<font color="blue">Please wait...</font>');
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url() . "stock_management/save"; ?>',
                    data: new FormData(this),
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    success: function (res) {
                        $("#tab_btn").removeAttr("disabled")
                        if (res.err === 0) {
                            document.getElementById("add_branda_form").reset();
                            $("#add_branda_form span").html('');
                            $("#alltab_err").html(res.msg);
							var newid = 'brandremove_'+res.id;
							if($("#"+newid).length>0){
								$("#"+newid).html("<td id='fruit_" + res.id + "'>" + res.fruit + "</td><td id='attr_" + res.id + "'>" + res.attr + "</td><td id='brand_" + res.id + "'>" + res.brand + "</td><td id='quantity_" + res.id + "'>" + res.quantity + "</td><td><a href='#' title='Edit' id='upbr_" + res.id + "' class='btn btn-xs btn-outline update_brand btn-success add-tooltip'><i class='fa fa-pencil'></i></a><a href='#' title='Delete' id='bdel_" + res.id + "' class='btn brand_delete btn-xs btn-outline btn-danger add-tooltip'><i class='fa fa-times'></i></a></td>");
							}
							else{
								$("#add_brand_tbl").prepend("<tr class='brand_remove_la' id='brandremove_" + res.id + "'><td id='fruit_" + res.id + "'>" + res.fruit + "</td><td id='attr_" + res.id + "'>" + res.attr + "</td><td id='brand_" + res.id + "'>" + res.brand + "</td><td id='quantity_" + res.id + "'>" + res.quantity + "</td><td><a href='#' title='Edit' id='upbr_" + res.id + "' class='btn btn-xs btn-outline update_brand btn-success add-tooltip'><i class='fa fa-pencil'></i></a><a href='#' title='Delete' id='bdel_" + res.id + "' class='btn brand_delete btn-xs btn-outline btn-danger add-tooltip'><i class='fa fa-times'></i></a></td></tr>");
							}
							
                            
                        } else if (res.err === 1 || res.err === 2) {
                            $("#alltab_err").html(res.msg);
                        }

                        // $("#add_brand_tbl").prepend(res.brand);
                    }
                });
            } else if ($("#type").val() === "edit") {
                $("#tab_btn").attr("disabled", "disabled")
                $("#branda_err").html('<font color="blue">Please wait...</font>');
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url() . "stock_management/edit"; ?>',
                    data: new FormData(this),
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    success: function (res) {
                        $("#tab_btn").removeAttr("disabled")
                        if (res.err === 0) {
                            $("#add_branda_form > span").html('');
                            //        alert(res.msg);
                            $("#str_"+res.map_id).text(res.quantity);
                            $("#branda_err").html(res.msg);
                        } else if (res.err === 1) {
                            $("#branda_err").html(res.msg);
                        }
                    }
                });
            }
            e.preventDefault();
            return false;
        });
    });
</script>


<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">Home</a></li>
    <li><a href="#">Operation</a></li>
    <li class="active"><a href="#">Sales List</a></li>
</ul>


<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Sales List</span>
            </div>
            <div class="panel-body">
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="col-xs-8">
                            <input type="text" name="salesdate" id="salesdate" class="form-control" placeholder="Date">
                        </div>
                        <div class="col-xs-4">
                            <input type="button" id="search_btn" value="Search" class="btn btn-primary btn-block">
                        </div>	
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <div class="col-xs-9"></div>
                        <div class="col-xs-3"><a href="<?= site_url('sales/add') ?>" class="btn btn-primary btn-labeled" style="width: 100%;"><span class="btn-label icon fa fa-plus"></span>Add New</a></div>	
                    </div>
                </div>

            </div>                
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">List of Sales</span>
            </div>
            <div class="panel-body">
                <div class="table-primary">
                    <table class="table table-bordered" id="salesList" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Edit/Delete</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table></div>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-heading">
             <span class="panel-title">Date : <span id="date_change"></span></span> <!-- <span class="panel-title" style="float:right;margin-top: -7px;"><input  type="text" id="search_attr" placeholder="Search Attribute" class="form-control"></span>-->
            </div>
            <div class="panel-body">
                <!-- Info table -->
                <div class="table-primary" id="sales_list">


                </div>
                <!-- / Info table -->
            </div>
        </div>
        <!-- /5. $CONTROLS -->
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#salesdate').datepicker({format: 'yyyy-mm-dd'});
        $("#search_btn").on('click', function (e) {
            var salesdate = $("#salesdate").val();
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "sales/getSalesList"; ?>',
                data: {salesdate: salesdate},
                dataType: "html",
                success: function (res) {
                    $("#salesList tbody").html(res);
                }
            });
        });
        $("#search_btn").trigger('click');


        $('html body').on('click', '.get_sales_view', function () {
            var id = $(this).attr("id");
            var d = id.split('_');
            $("#date_change").text($(this).text());
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "sales/get_sales_id"; ?>',
                data: {id: d[1]},
                dataType: "html",
                success: function (res) {
                    $("#sales_list").html(res);
                }
            });
        });

        $('html body').on('click', '.update_sales_list', function () {
            var id = $(this).attr("id");
			var txt =$(this).parent('td').siblings('.get_sales_view').text();
			$("#date_change").text(txt);
            var d = id.split('_');
			$("#sales_list").html('<img src="<?=base_url()?>admin_asset/assets/images/loader.gif">');
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "sales/edit_sales_list"; ?>',
                data: {id: d[1]},
                dataType: "html",
                success: function (res) {
					//errorDiv
					$("#sales_list").html(res);
                }
            });
            return false;
        });

        $('html body').on('submit', '#edit_sales_list_form', function () {
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "sales/edit_sales_list_id"; ?>',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (res) {
					if(res=='')
					$("#errorDiv").html('Sales List for '+$('#date_change').text()+' has updated.');
					else
					$("#errorDiv").html('There is an error, please try again')
                }
            });
            return false;
        });

        $("html body").on('click', '#checkAll', function () {

            if ($(this).prop('checked')) {
                console.log('true')
                $(".frm tbody input:checkbox").each(function () {
                    //$(this).attr('checked',true);
                    $(this).prop('checked', true);
                });
            } else {
                console.log('false')
                $(".frm tbody input:checkbox").each(function () {
                    //$(this).removeAttr('checked');
                    $(this).prop('checked', false);
                });
                //$(this).attr('checked',false);	
            }
        });
    });
</script>
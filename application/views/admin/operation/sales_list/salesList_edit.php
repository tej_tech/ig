<div class="row col-xs-12 has-error"><div class="control-label" id="errorDiv"></div></div>
<form id="edit_sales_list_form" name="edit_sales_list_form">
    <table class="table frm">
        <thead>
            <tr>
                <th width="5%"><input type="checkbox" id="checkAll" /></th>
                <th width="15%">Fruit</th>
                <th width="20%">Brand</th>
                <th width="25%">Attribute</th><th width="10%">Unit Count</th>
                <th width="25%">Price</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($maplist as $m) { ?>
                <tr>
                    <td><input type="checkbox" <?php
                        if (in_array($m['id'], $f)) {
                            echo "checked";
                        }
                        ?> name="addThis[<?php echo $m['id']; ?>]" value="<?= $m['id'] ?>" /></td>
                    <td><?php echo stripslashes($m['name']) ?></td>
                    <td><?php echo stripslashes($m['b_name']) ?></td>
                    <td><?php echo stripslashes($m['attr_name']) ?></td><td><?=$m['unit_count']?></td>
                    <td><?php
                        if ($m['unit_price_s'] == 1)
                            echo '<span class="col-sm-5 no-padding"><input class="form-control priceonly" name="unit_price1[' . $m['id'] . ']" type="text" value="' . $m['unit_price1'] . '">' . '</span><span class="col-sm-2">to</span><span class="col-sm-5 no-padding"><input class="form-control priceonly" name="unit_price2[' . $m['id'] . ']" type="text" value="' . $m['unit_price2'] . '"></span>';
                        else
                            echo '<input class="form-control priceonly" name="unit_price1[' . $m['id'] . ']" type="text" value="' . $m['unit_price1'] . '"><input class="form-control priceonly" name="unit_price2[' . $m['id'] . ']" type="hidden" value="0.00">';
                        ?></td>
                </tr>
            <?php } ?> 
            <tr><th colspan="3" style="text-align:right">Sales List Date :</th><td><input type="text" class="form-control" name="sales_date" id="sales_date" value="<?php echo $xm[0]['sales_date']; ?>" required="required" /></td><td></td><td>
                    <input type="hidden" name="ids" value="<?php echo $ids; ?>" />
                    <input type="hidden" name="arr" value="<?php echo trim(implode($f, ','),','); ?>" />
                    <button type="submit" name="edit_list" id="edit_list" class="btn btn-primary">Update</button>
                    <a href="<?= site_url('sales/lists') ?>" class="btn">Cancel</a></td></tr>
        </tbody>   
    </table>
</form>
<script>
    $(document).ready(function () {
        $('#sales_date').datepicker({format: 'yyyy-mm-dd'});
    });
</script>
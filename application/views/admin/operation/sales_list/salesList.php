<?php
if(sizeof($sales)>0){
	$cnt=1;
	foreach ($sales as $b) { ?>
        <tr id="brandremove_<?php echo $b['id']; ?>">
        	<td><?=$cnt?></td>
            <td class='get_sales_view' id="en_<?php echo $b['id']; ?>"><?php echo date("d M Y",strtotime($b['sales_date'])); ?></td>
            <td>
                <a href="#" title="Edit" id="upbr_<?php echo $b['id']; ?>" class="btn btn-xs btn-outline update_sales_list"><i class="fa fa-pencil"></i></a>
                <a href="#" title="Delete" id="bdel_<?php echo $b['id']; ?>" class="btn brand_delete btn-xs btn-outline"><i class="fa fa-times"></i></a>
            </td>
        </tr>
    <?php $cnt++;
	}
}
else{
	echo '<tr><td colspan="2">There are not any matching records</td></tr>';	
}
?>
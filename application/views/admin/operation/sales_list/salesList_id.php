                            
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Fruit</th>
            <th>Attribute</th>
            <th>Brand</th>
            <th>Price</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (is_array($l)) {
            foreach ($l as $b) {
                ?>
                <tr class="brand_remove_la" id="brandremove_<?php echo $b['id']; ?>">
                    <td id="str_<?php echo $b['id']; ?>"><?php echo $b['name']; ?></td>
                    <td id="ec_<?php echo $b['id']; ?>"><?php echo $b['attr_name']; ?></td>
                    <td id="ens_<?php echo $b['id']; ?>"><?php echo $b['b_name']; ?></td>
                    <td id="end_<?php echo $b['id']; ?>"><?php
                        echo $b['price1'];
                        if ($b['price2'] > 0) {
                            echo " - " . $b['price2'];
                        }
                        ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>

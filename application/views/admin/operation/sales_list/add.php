<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">Home</a></li>
    <li><a href="#">Operation</a></li>
    <li><a href="#">Sales List</a></li><li class="active"><a href="#">Add New</a></li>
</ul>
<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Sales List &nbsp; &raquo; Add New</span>
            </div>
            <div class="panel-body">
            <?php echo validation_errors(); 
			?>
        <div class="row col-xs-12" id="errorDiv"><?=$msg?></div>
        <?php echo form_open('sales/add', 'class="form-horizontal" id="frm"'); //echo form_open('form'); ?>
                <table class="table">
                <thead>
                    <tr>
                        <th width="5%"><input type="checkbox" id="checkAll" /></th><th width="15%">Fruit</th><th width="20%">Brand</th>
                        <th width="25%">Attribute</th><th width="10%">Unit Count</th><th width="25%">Price</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($maplist as $m){ ?>
                
                <tr>
                	<td><input type="checkbox" name="addThis[]" value="<?=$m['id']?>" /></td><td><?php echo stripslashes($m['name']) ?></td>
            <td><?php echo stripslashes($m['b_name']) ?></td>
            <td><?php echo stripslashes($m['attr_name']) ?></td>
            <td><?=$m['unit_count']?></td>
            <td><?php
			if($m['unit_price_s']==1)
			echo '<span class="col-sm-5 no-padding"><input class="form-control priceonly" name="unit_price1['.$m['id'].']" type="text" value="'.$m['unit_price1'].'">'.'</span><span class="col-sm-2">to</span><span class="col-sm-5 no-padding"><input class="form-control priceonly" name="unit_price2['.$m['id'].']" type="text" value="'.$m['unit_price2'].'"></span>';
			else echo '<input class="form-control priceonly" name="unit_price1['.$m['id'].']" type="text" value="'.$m['unit_price1'].'">'; ?></td>
            
                </tr>
                <?php } ?> 
                <tr><th colspan="4" style="text-align:right">Sales List Date :</th><td><input type="text" class="form-control" name="sales_date" id="sales_date" required="required" /></td><td>
                <input type="hidden" name="action" value="newSale" />
                <button class="btn btn-primary btn-labeled"><span class="btn-label icon fa fa-plus"></span>Add</button>
                <a href="<?=site_url('sales/lists')?>" class="btn">Cancel</a></td></tr>
                </tbody>   
                </table>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function () {
	$('#sales_date').datepicker({format: 'yyyy-mm-dd'});
	$("#checkAll").on('click',function(){
		
		if($(this).prop('checked')){
			console.log('true')
			$("#frm tbody input:checkbox").each(function(){
				//$(this).attr('checked',true);
				$(this).prop('checked',true);
			});
		}
		else{
			console.log('false')
			$("#frm tbody input:checkbox").each(function(){
				//$(this).removeAttr('checked');
				$(this).prop('checked',false);
			});
			//$(this).attr('checked',false);	
		}
	});
	$("#frm").on('submit', function (e) {
		$("#submit_btn").attr("disabled", "disabled");	
	});
});
</script>
<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">Home</a></li>
    <li><a href="#">Operation</a></li>
    <li class="active"><a href="#">Item / Tabs</a></li>
</ul>

<div>
    <div class="row">
        <div class="col-sm-4">
            <form class="form-horizontal switch_th"  id="add_branda_form" method="post">
                <div class="row">
                    <div class="col-xs-12">
                        <select name="store" id="store" class="form-control form-group-margin">
                            <option></option>
                            <?php foreach ($store as $t) { ?>
                                <option value="<?php echo $t['id']; ?>"><?php echo $t['store_name']; ?></option>
                            <?php } ?>
                        </select>
                        <span id="store_err"></span>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="tab_name"  id="tab_name" class="form-control input-md" placeholder="Brand name">
                                <span id="tab_name_err"></span>
                            </div>
                        </div>
                        <script>
                            init.push(function () {
                                $('#store').select2({allowClear: true, placeholder: 'Select Store...'}).change(function () {
                                    $(this).valid();
                                });
                                var options = {
                                    todayBtn: "linked",
                                    orientation: $('body').hasClass('right-to-left') ? "auto right" : 'auto auto'
                                }
                                $('#date_picker').datepicker(options);
                            });
                        </script>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="date_picker"  id="date_picker" class="form-control input-md" placeholder="Select date">
                                <span id="date_err"></span>
                            </div>
                        </div>
                    </div> 
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="price"  id="price" class="form-control input-md" placeholder="Price">
                                <span id="price_err"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-sm-12 button_at">
                                <input type='hidden' name='type' id="type" value='new'>
                                <input type="submit" id="tab_btn" value="Add Tab" class="btn btn-primary btn-md btn-block">
                                <span id="alltab_err"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-8">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">Item / brands Attribute</span> <span class="panel-title" style="float:right;margin-top: -7px;"><input  type="text" id="search_attr" placeholder="Search Attribute" class="form-control"></span>
                </div>
                <div class="panel-body">
                    <!-- Info table -->
                    <div class="table-primary">

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Store Name</th>
                                    <th>Tab Model</th>
                                    <th>Price</th>
                                    <th>Purchase Date</th>
                                    <th>Edit/Delete</th>
                                </tr>
                            </thead>
                            <tbody id="add_brand_tbl">
                                <?php
                                if (is_array($tab)) {
                                    foreach ($tab as $b) {
                                        ?>
                                        <tr class="brand_remove_la" id="brandremove_<?php echo $b['id']; ?>">
                                            <td id="date_<?php echo $b['id']; ?>"><?php echo $this->tm->get_storeName($b['store_id']); ?></td>
                                            <td id="str_<?php echo $b['id']; ?>"><?php echo $b['model_code']; ?></td>
                                            <td id="ec_<?php echo $b['id']; ?>"><?php echo $b['price']; ?></td>
                                            <td id="en_<?php echo $b['id']; ?>"><?php echo $b['purchase_date']; ?></td>
                                            <td>
                                                <a href="#" title="Edit" id="upbr_<?php echo $b['id']; ?>" class="btn btn-xs btn-outline update_brand btn-success add-tooltip"><i class="fa fa-pencil"></i></a>
                                                <a href="#" title="Delete" id="bdel_<?php echo $b['id']; ?>" class="btn brand_delete btn-xs btn-outline btn-danger add-tooltip"><i class="fa fa-times"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- / Info table -->
                </div>
            </div>
            <!-- /5. $CONTROLS -->
        </div>
    </div>
</div> <!-- / #content-wrapper -->
<script>
    $(document).ready(function () {

        $("#search_attr").keyup(function () {
            var sdt = "s_name=" + $(this).val();
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "tab/get_tab_search"; ?>',
                data: sdt,
                success: function (resd) {
                    $("#add_brand_tbl").html(resd);
                }
            });
        });


        $("html body").on("click", ".update_brand", function () {
            var fg = $(this).attr("id");
            var f = fg.split("_");
            var sdt = "id=" + f[1];
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "tab/get_tab_id"; ?>',
                dataType: "json",
                data: sdt,
                success: function (res) {
                    $("#tab_name").val(res.model_code);
                    $("#date_picker").val(res.purchase_date);
                    $("#price").val(res.price);
                    $(".button_at").html("<input type='hidden' name='type' id='type'  value='edit'><input type='hidden' id='id' name='id' value='" + res.id + "'><input type='submit' id='upbranda_btn' value='Update brand Attribute' class='btn btn-primary btn-md btn-block'><span id='branda_err'></span><a href='#' id='change_to_add_attr' value='Add brand Attribute' class='pull-right'>Add new Attribute</a>");
                    $("#store").val(res.store_id).change();
                }
            });
        });

        $("html body").on("click", "#change_to_add_attr", function () {
            document.getElementById("add_branda_form").reset();
            $("#store").html('<option></option><?php foreach ($store as $t) { ?><option value="<?php echo $t['id']; ?>"><?php echo $t['store_name']; ?></option><?php } ?>');
            $("#add_branda_form span").html('');
            $("#aname").attr("name", "aname");
            $("#aname").removeAttr("disabled");
            $(".button_at").html("<input type='hidden' name='type' id='type' value='new'><input type='submit' id='branda_btn' value='Add brand Attribute' class='btn btn-primary btn-md btn-block'><span id='branda_err'></span>");
        });


        $("#add_brand_tbl").on("click", ".brand_delete", function () {
            if (confirm("Are you sure you want to delete?")) {
                var f = $(this).attr("id");
                var d = f.split("_");
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url() . "tab/delete"; ?>',
                    data: 'id=' + d[1],
                    success: function (res) {
                        $("#brandremove_" + res).remove();
                    }
                });
            }
            return false;
        });


        // $("#tab_name").keyup(check_tabname);
        $("#tab_name").blur(check_tabname);
        //$("#store").change(validate_store);
        //$("#date_picker").keyup(check_date);
        $("#date_picker").blur(check_date);
        // $("#price").keyup(check_price);
        $("#price").blur(check_price);

//        function check_store_tab(store_id, tab_model) {
//            $.ajax({
//                type: 'post',
//                url: '<?php echo base_url() . "tab/validate"; ?>',
//                dataType: "json",
//                data: 'store=' + store_id + "&tab_name=" + tab_model,
//                success: function (res) {
//                    //conole.log()
//                    if (res.err === 2) {
//                        $("#store_err").parent().addClass('has-error');
//                        $("#store_err").html('<font color="red">Store name against tab name already exist</font>');
//                        $("#tab_name_err").parent().addClass('has-error');
//                        $("#tab_name_err").html('<font color="red">Store name against tab name already exist</font>');
//                        return false;
//                    } else if (res.err === 0) {
//                        $("#store_err").parent().removeClass('has-error');
//                        $("#store_err").html('');
//                        $("#tab_name_err").parent().removeClass('has-error');
//                        $("#tab_name_err").html('');
//
//                        return false;
//                    } else {
//                        $("#store_err").parent().removeClass('has-error');
//                        $("#store_err").html('');
//                        $("#tab_name_err").parent().removeClass('has-error');
//                        $("#tab_name_err").html('');
//                        return false;
//                    }
//                }
//            });
//        }


        function validate_store() {
            var f = $("#store").val();
            if (f.length !== 0) {
                $("#store_err").parent().removeClass('has-error');
                $("#store_err").html('');
                return true;
            } else {
                $("#store_err").parent().addClass('has-error');
                $("#store_err").html('<font color="red">Please select store</font>');
                return false;
            }
        }

        function check_date() {
            var f = $("#date_picker").val();
            if (f.length !== 0) {
                $("#date_err").parent().removeClass('has-error');
                $("#date_err").html('');
                return true
            } else {
                $("#date_err").parent().addClass('has-error');
                $("#date_err").html('<font color="red">Please select date</font>');
                return false;
            }
        }

        function check_tabname() {
            if ($("#tab_name").val().length === 0) {
                $("#tab_name_err").parent().addClass('has-error');
                $("#tab_name_err").html('<font color="red">Required field</font>');
                return false;
            } else {
                $("#tab_name_err").parent().removeClass('has-error');
                $("#tab_name_err").html('');
                return true;
            }
        }

        function check_price() {
            if ($("#tab_name").val().length === 0) {
                $("#tab_name_err").parent().addClass('has-error');
                $("#tab_name_err").html('<font color="red">Required field</font>');
                return false;
            } else {
                $("#tab_name_err").parent().removeClass('has-error');
                $("#tab_name_err").html('<font color="green">Done</font>');
                return true;
            }
        }

        $("#add_branda_form").on('submit', function (e) {
            if ($("#type").val() === "new") {
                if (check_tabname() & validate_store() & check_date() & check_price()) {
                    $("#tab_btn").attr("disabled", "disabled")
                    $("#alltab_err").html('<font color="blue">Please wait...</font>');
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url() . "tab/save_item_tab"; ?>',
                        data: new FormData(this),
                        dataType: "json",
                        processData: false,
                        contentType: false,
                        success: function (res) {
                            $("#tab_btn").removeAttr("disabled")
                            if (res.err == 0) {
                                $("#alltab_err").html('<font color="green">Tab saved successfully</font>');
                                document.getElementById("add_branda_form").reset();
                                $("#add_branda_form span").html('');
                            } else if (res.err == 1) {
                                $("#alltab_err").html('<font color="red">Fill all fields properly</font>');
                            }

                            //   $("#add_brand_tbl").prepend(res.brand);
                        }
                    });
                } else {
                    $("#brand_err").html('<font color="red">Fill all fields properly</font>');
                }
            } else if ($("#type").val() === "edit") {
                if (validate_store() & check_date() & check_price()) {
                    $("#tab_btn").attr("disabled", "disabled")
                    $("#alltab_err").html('<font color="blue">Please wait...</font>');
                    $.ajax({
                        type: 'post',
                        url: '<?php echo base_url() . "tab/edit_item_tab"; ?>',
                        data: new FormData(this),
                        dataType: "json",
                        processData: false,
                        contentType: false,
                        success: function (res) {
                            $("#tab_btn").removeAttr("disabled")
                            if (res.err === 0) {
                                $("#alltab_err").html('<font color="green">Tab updated successfully</font>');
                                $("#add_branda_form > span").html('');
                            } else if (res.err === 1) {
                                $("#alltab_err").html('<font color="red">Fill all fields properly</font>');
                            }
                        }
                    });
                } else {
                    $("#brand_err").html('<font color="red">Fill all fields properly</font>');
                }
            }
            e.preventDefault();
            return false;
        });
    });
</script>
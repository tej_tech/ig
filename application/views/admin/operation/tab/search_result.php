<?php
if (is_array($tab)) {
    foreach ($tab as $b) {
        ?>
        <tr class="brand_remove_la" id="brandremove_<?php echo $b['id']; ?>">
            <td id="date_<?php echo $b['id']; ?>"><?php echo $this->tm->get_storeName($b['store_id']); ?></td>
            <td id="str_<?php echo $b['id']; ?>"><?php echo $b['model_code']; ?></td>
            <td id="ec_<?php echo $b['id']; ?>"><?php echo $b['price']; ?></td>
            <td id="en_<?php echo $b['id']; ?>"><?php echo $b['purchase_date']; ?></td>
            <td>
                <?php  ?><a href="#" title="Edit" id="upbr_<?php echo $b['id']; ?>" class="btn btn-xs btn-outline update_brand btn-success add-tooltip"><i class="fa fa-pencil"></i></a>
                  <a href="#" title="Delete" id="bdel_<?php echo $b['id']; ?>" class="btn brand_delete btn-xs btn-outline btn-danger add-tooltip"><i class="fa fa-times"></i></a><?php  ?>
            </td>
        </tr>
        <?php
    }
}
?>

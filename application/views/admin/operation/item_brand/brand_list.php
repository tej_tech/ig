<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">Home</a></li>
    <li><a href="#">Operation</a></li>
    <li class="active"><a href="#">Item / brands list</a></li>
</ul>

<div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">
                <div class="panel">
                    <div class="panel-body">
                <!-- Info table -->
                        <div class="table-primary">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Brand Name</th>
                                        <th>Brand Id</th>
                                        <th>Edit/Delete</th>
                                    </tr>
                                </thead>
                                <tbody id="add_brand_tbl">
                                    <?php foreach ($brand as $b) { ?>
                                        <tr id="brandremove_<?php echo $b['id']; ?>">
                                            <td id="en_<?php echo $b['id']; ?>"><?php echo $b['b_name']; ?></td>
                                            <td id="ec_<?php echo $b['id']; ?>"><?php echo $b['b_code']; ?></td>
                                            <td>
                                                <a href="<?php echo base_url("brand/edit")."/".$b['id']; ?>" title="Edit" id="upbr_<?php echo $b['id']; ?>" class="btn btn-xs btn-outline update_brand btn-success add-tooltip"><i class="fa fa-pencil"></i></a>
                                                <a href="#" title="Delete" id="bdel_<?php echo $b['id']; ?>" class="btn brand_delete btn-xs btn-outline btn-danger add-tooltip"><i class="fa fa-times"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- / Info table -->
                    </div>
                </div>

            </div>
            <!-- /5. $CONTROLS -->
        </div>
    </div>
</div> <!-- / #content-wrapper -->
<script>
    $(document).ready(function () {

        $("#add_brand_tbl").on("click", ".brand_delete", function () {
            if (confirm("Are you sure you want to delete?")) {
                var f = $(this).attr("id");
                var d = f.split("_");
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url() . "item_brand/delete_brand"; ?>',
                    data: 'id=' + d[1],
                    success: function (res) {
                        $("#brandremove_" + res).remove();
                    }
                });
            }
            return false;
        });
    });
</script>
<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">Home</a></li>
    <li><a href="#">Operation</a></li>
    <li class="active"><a href="#">Item / brands</a></li>
</ul>

<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Item / brands</span>
            </div>
            <div class="panel-body">
                <form class="form-horizontal"  id="add_brand_form" method="post">
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="form-group">

                                <div class="col-sm-12">
                                    <input type="text" name="bname" id="bname" class="form-control input-lg" placeholder="Brand name">
                                    <span id="bname_err"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" name="bcode" id="bcode" class="form-control input-lg" placeholder="Brand Code">
                                    <span id="bcode_err"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="submit" id="brand_btn" value="Add brand" class="btn btn-primary btn-lg btn-block">
                                    <span id="brand_err"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!-- Info table -->
                <div class="table-primary">

                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Brand Name</th>
                                <th>Brand Id</th>
                                <th>Edit/Delete</th>
                            </tr>
                        </thead>
                        <tbody id="add_brand_tbl">
                            <?php foreach ($brand as $b) { ?>
                                <tr id="brandremove_<?php echo $b['id']; ?>">
                                    <td id="en_<?php echo $b['id']; ?>"><?php echo $b['b_name']; ?></td>
                                    <td id="ec_<?php echo $b['id']; ?>"><?php echo $b['b_code']; ?></td>
                                    <td>
                                        <a href="#" title="Edit" data-toggle="modal" data-target="#myModal1" id="upbr_<?php echo $b['id']; ?>" class="btn btn-xs btn-outline update_brand btn-success add-tooltip"><i class="fa fa-pencil"></i></a>
                                        <a href="#" title="Delete" id="bdel_<?php echo $b['id']; ?>" class="btn brand_delete btn-xs btn-outline btn-danger add-tooltip"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- / Info table -->
            </div>
        </div>
        <!-- Modal -->
        <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Brand</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal"  id="update_brand_form" method="post">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="text" id="upname" readonly="" class="form-control input-lg" placeholder="Brand name">
                                            <span id="upbname_err"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <div class="col-sm-12"> 
                                            <input type="text" name="bcode" id="upbcode" value="" class="form-control input-lg" placeholder="Brand Code">
                                            <input type="hidden" name="id_brand" id="id_brand">
                                            <span id="upbcode_err"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="submit" id="upbrand_btn" value="Update brand" class="btn upbrand_ btn-primary btn-lg btn-block">
                                            <span class="up_branderr" id=""></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div> <!-- / .modal-body -->
                </div> <!-- / .modal-content -->
            </div> <!-- / .modal-dialog -->
        </div> <!-- /.modal -->
        <!-- / Modal -->
    </div>
</div>
<script>
    $(document).ready(function () {

        $("html body").on("click", ".update_brand", function () {
            var fg = $(this).attr("id");
            var f = fg.split("_");
            var sdt = "id=" + f[1];
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "item_brand/get_brand_id"; ?>',
                dataType: "json",
                data: sdt,
                success: function (res) {

                    $("#id_brand").val(res.id);
                    $("#upname").val(res.bname);
                    $("#upbcode").val(res.bcode);
                    $(".up_branderr").html("");
                    $(".up_branderr").attr("id", "upbranderr_" + res.id);
                }
            });
        });

        $("html body").on("submit", "#update_brand_form", function (e) {
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "item_brand/update_brand"; ?>',
                data: new FormData(this),
                dataType: "json",
                processData: false,
                contentType: false,
                success: function (res) {
                    $("#ec_" + res.id).text(res.bcode);
                    $("#upbranderr_" + res.id).html("<font color=\"green\">Updated Successfully<font>");
                }
            });
            e.preventDefault();
            return false;
        });

        $("#add_brand_tbl").on("click", ".brand_delete", function () {
            if (confirm("Are you sure you want to delete?")) {
                var f = $(this).attr("id");
                var d = f.split("_");
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url() . "item_brand/delete_brand"; ?>',
                    data: 'id=' + d[1],
                    success: function (res) {
                        $("#brandremove_" + res).remove();
                    }
                });
            }
            return false;
        });


        $("#bname").keyup(check_bname);
        $("#bname").blur(check_bname);
        $("#bcode").keyup(check_bcode);
        $("#bcode").blur(check_bcode);


        function brand_ajax(brand_name) {
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "item_brand/validate_brand_name"; ?>',
                data: 'bname=' + brand_name,
                success: function (res) {
                    if (res === "0") {
                        $("#bname_err").parent().removeClass('has-error');
                        $("#bname_err").html('<font color="green">Done</font>');
                        return true;
                    } else if (res === "1") {
                        $("#bname_err").parent().addClass('has-error');
                        $("#bname_err").html('<font color="red">Brand name already used</font>');
                        return false;
                    }
                }
            });
        }

        function check_bname() {
            if ($("#bname").val().length == 0) {
                $("#bname_err").parent().addClass('has-error');
                $("#bname_err").html('<font color="red">Required field</font>');
                return false;
            } else {
                brand_ajax($("#bname").val());
                return true;
            }
        }

        function check_bcode() {
            if ($("#bcode").val().length == 0) {
                $("#bcode_err").parent().addClass('has-error');
                $("#bcode_err").html('<font color="red">Required field</font>');
                return false;
            } else {
                $("#bcode_err").parent().removeClass('has-error');
                $("#bcode_err").html('<font color="green">Done</font>');
                return true;
            }
        }


        $("#add_brand_form").on('submit', function (e) {
            if (check_bname() & check_bcode()) {
                $("#brand_btn").attr("disabled", "disabled")
                $("#brand_err").html('<font color="blue">Please wait...</font>');
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url() . "item_brand/save_item_brand"; ?>',
                    data: new FormData(this),
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    success: function (res) {
                        document.getElementById("add_brand_form").reset();
                        $("#brand_btn").removeAttr("disabled")
                        $("#brand_err").html(res.msg);
                        $("#add_brand_tbl").prepend(res.brand);
                        if (res.id == 0) {
                            $("#add_brand_form span").html('');
                        }
                    }
                });
            } else {
                $("#brand_err").html('<font color="red">Fill all fields properly</font>');
            }
            e.preventDefault();
            return false;
        });
    });
</script>
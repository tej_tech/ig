<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">Home</a></li>
    <li><a href="#">Operation</a></li>
    <li class="active"><a href="#">Item / brands</a></li>
</ul>

<div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">Item / brands</span>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal"  id="add_brand_form" method="post">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="bname" class="col-sm-3 control-label">Brand name</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="bname" id="bname" class="form-control input-lg" placeholder="Brand name">
                                            <span id="bname_err"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="bcode" class="col-sm-3 control-label">Brand Code</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="bcode" id="bcode" class="form-control input-lg" placeholder="Brand Code">
                                            <span id="bcode_err"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-4">
                                        <input type="submit" id="brand_btn" value="Add brand" class="btn btn-primary btn-lg btn-block">
                                        <span id="brand_err"></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- /5. $CONTROLS -->
        </div>
    </div>
</div> <!-- / #content-wrapper -->
<script>
    $(document).ready(function () {

        $("#bname").keyup(check_bname);
        $("#bname").blur(check_bname);
        $("#bcode").keyup(check_bcode);
        $("#bcode").blur(check_bcode);


        function brand_ajax(brand_name) {
            $.ajax({
                type: 'post',
                url: '<?php echo base_url() . "brand/validate_brand_name"; ?>',
                data: 'bname=' + brand_name,
                success: function (res) {
                    if (res === "0") {
                        $("#bname_err").parent().removeClass('has-error');
                        $("#bname_err").html('<font color="green">Done</font>');
                        return true;
                    } else if (res === "1") {
                        $("#bname_err").parent().addClass('has-error');
                        $("#bname_err").html('<font color="red">Brand name already used</font>');
                        return false;
                    }
                }
            });
        }

        function check_bname() {
            if ($("#bname").val().length == 0) {
                $("#bname_err").parent().addClass('has-error');
                $("#bname_err").html('<font color="red">Required field</font>');
                return false;
            } else {
                brand_ajax($("#bname").val());
                return true;
            }
        }

        function check_bcode() {
            if ($("#bcode").val().length == 0) {
                $("#bcode_err").parent().addClass('has-error');
                $("#bcode_err").html('<font color="red">Required field</font>');
                return false;
            } else {
                $("#bcode_err").parent().removeClass('has-error');
                $("#bcode_err").html('<font color="green">Done</font>');
                return true;
            }
        }


        $("#add_brand_form").on('submit', function (e) {
            if (check_bname() & check_bcode()) {
                $("#brand_btn").attr("disabled", "disabled")
                $("#brand_err").html('<font color="blue">Please wait...</font>');
                $.ajax({
                    type: 'post',
                    url: '<?php echo base_url() . "brand/save_item_brand"; ?>',
                    data: new FormData(this),
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    success: function (res) {
                        $("#brand_btn").removeAttr("disabled")
                        $("#brand_err").html(res.msg);
                        if (res.id == 0) {
                            document.getElementById("add_brand_form").reset();
                            $("#add_brand_form span").html('');
                        }
                    }
                });
            } else {
                $("#brand_err").html('<font color="red">Fill all fields properly</font>');
            }
            e.preventDefault();
            return false;
        });
    });
</script>
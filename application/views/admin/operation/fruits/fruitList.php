<?php
if(sizeof($fruits)>0){
	foreach ($fruits as $b) { ?>
        <tr id="brandremove_<?php echo $b['id']; ?>">
            <td id="en_<?php echo $b['id']; ?>"><?php echo $b['name']; ?></td>
            <td>
                <a href="#" title="Edit" data-toggle="modal" data-target="#myModal1" id="upbr_<?php echo $b['id']; ?>" class="btn btn-xs btn-outline update_brand btn-success add-tooltip"><i class="fa fa-pencil"></i></a>
                <a href="#" title="Delete" id="bdel_<?php echo $b['id']; ?>" class="btn brand_delete btn-xs btn-outline btn-danger add-tooltip"><i class="fa fa-times"></i></a>
            </td>
        </tr>
    <?php }
}
else{
	echo '<tr><td colspan="2">There are not any matching records</td></tr>';	
}
?>
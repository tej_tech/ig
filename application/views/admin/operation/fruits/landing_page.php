<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">Home</a></li>
    <li><a href="#">Operation</a></li>
    <li class="active"><a href="#">Fruits</a></li>
</ul>
<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Fruits</span>
            </div>
            <div class="panel-body">
                <div class="row col-sm-6">
                <form class="form-horizontal" id="add_fruit_form" method="post">
                    <div class="row form-group">
                        <div class="col-xs-6">
                            
                                    <input type="text" name="fname" id="fname" class="form-control input-lg" placeholder="Fruit name">
                                    <span id="fname_err"></span>
                            
                        </div>
                        
                        <div class="col-xs-6">
                            
                                    <input type="submit" id="brand_btn" value="Add Fruit" class="btn btn-primary btn-lg btn-block">
                                    <span id="brand_err"></span>
                            
                        </div>
                    </div>
                </form>
                </div>
                <div class="row col-sm-6">
                    <div class="form-group">
                        <div class="col-xs-6">
                                <input type="text" name="fname" id="fruitname" class="form-control input-lg" placeholder="Fruit name">
                        </div>
                        <div class="col-xs-6">
                                <input type="button" id="search_btn" value="Search" class="btn btn-primary btn-lg btn-block">
                        </div>	
                    </div>
                </div>
                <!-- Info table -->
                <div class="table-primary">

                    <table class="table table-bordered" id="fruitList">
                        <thead>
                            <tr>
                                <th>Fruit Name</th>
                                <th>Edit/Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
                <!-- / Info table -->
            </div>
        </div>
        <!-- /5. $CONTROLS -->
        <!-- Modal -->
        <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">Edit Fruit</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal"  id="update_fruit_form" method="post">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="text" id="upname" class="form-control input-lg" placeholder="Fruit name">
                                            <span id="upfname_err"></span>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="submit" id="upbrand_btn" value="Update Fruit" class="btn upbrand_ btn-primary btn-lg btn-block">
                                            <span class="up_branderr" id=""></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div> <!-- / .modal-body -->
                </div> <!-- / .modal-content -->
            </div> <!-- / .modal-dialog -->
        </div> <!-- /.modal -->
        <!-- / Modal -->

    </div>
</div>
<script>
$(document).ready(function () {
	//getFruitList
	$("#search_btn").on('click', function (e) {
		var searchStr = $("#fruitname").val();
		$.ajax({
				type: 'post',
				url: '<?php echo base_url() . "fruits/getFruitList"; ?>',
				data: {searchStr:searchStr},
				dataType: "html",
				success: function (res) {
					$("#fruitList tbody").html(res);
				}
			});
	});
	$("#search_btn").trigger('click');
	$("#add_fruit_form").on('submit', function (e) {
		 if ($("#fname").val().length > 0) {
			//console.log($("#fname").val());
			$("#brand_btn").attr("disabled", "disabled")
			$("#brand_err").html('<font color="blue">Please wait...</font>');
			$.ajax({
				type: 'post',
				url: '<?php echo base_url() . "fruits/save_fruits"; ?>',
				data: new FormData(this),
				dataType: "json",
				processData: false,
				contentType: false,
				success: function (res) {
					document.getElementById("add_fruit_form").reset();
					$("#brand_btn").removeAttr("disabled")
					$("#brand_err").html(res.msg);
					/*$("#add_brand_tbl").prepend(res.brand);
					if (res.id == 0) {
						$("#add_fruit_form span").html('');
					}*/
					$("#search_btn").trigger('click');
				}
			});
		} else {
			$("#brand_err").html('<font color="red">Fill all fields properly</font>');
		}
		e.preventDefault();
		return false;
	});
});
</script>
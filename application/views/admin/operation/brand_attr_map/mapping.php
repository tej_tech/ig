<ul class="breadcrumb breadcrumb-page">
    <div class="breadcrumb-label text-light-gray">You are here: </div>
    <li><a href="#">Home</a></li>
    <li><a href="#">Operation</a></li>
    <li class="active"><a href="#">Mapping</a></li>
</ul>
<div class="row">
    <div class="col-sm-4">
    	<?php //echo validation_errors(); ?>
        <div class="row col-xs-12" id="errorDiv"></div>
        <?php echo form_open('mapping', 'class="form-horizontal" id="frm"'); //echo form_open('form'); ?>
        <!--<form class="form-horizontal switch_th" id="frm" method="post">-->
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="col-sm-12">
                        <select class="form-control js-example-placeholder-single" name="fruit_id" id="fruit_id" required><option></option>
                        <?php
						foreach($fruits as $fruit){?><option value="<?=$fruit['id']?>"><?=stripslashes($fruit['name'])?></option><? }?>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="col-sm-12">
                        <select class="form-control js-example-placeholder-single" name="brand_id" id="brand_id" required><option></option>
                        <?php
						foreach($brands as $brand){?><option value="<?=$brand['id']?>"><?=stripslashes($brand['b_name'])?></option><? }?>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="col-sm-12">
                        <select class="form-control js-example-placeholder-single" name="attr_id" id="attr_id" required><option></option>
                        <?php
						foreach($brand_attrs as $attr){?><option value="<?=$attr['id']?>"><?=stripslashes($attr['attr_name'])?></option><? }?>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="col-sm-4">Unit Price</label>
                        <div class="col-sm-8"><input type="radio" name="unit_price_s" value="0" required /> Single &nbsp;
                        <input type="radio" name="unit_price_s" value="1" required /> Range</div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group" id="priceDiv">
                        
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="col-sm-12"><input type="text" class="form-control numberonly" name="unit_count" value="" placeholder="Unit Count" required /></div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="col-sm-12 button_at">
                            <input type='hidden' name='subtype' id="subtype" value='new'>
                            <input type="submit" value="Add Mapping" id="add_mapping" class="btn btn-primary btn-md btn-block">
                            <span id="branda_err"></span>
                        </div>
                    </div>
                </div>
            </div>
        </form><div id="loadList" style="display:none"></div>
    </div>
    <div class="col-sm-8">
        <div class="panel">
            <div class="panel-heading">
                <span class="panel-title">Mapping List</span>
            </div>
            <div class="panel-body">
                <!-- Info table -->
                <div class="table-primary">

                    <table class="table table-bordered" id="mapping_list">
                        <thead>
                            <tr>
                                <th>#</th><th>Fruit</th><th>Brand</th>
                                <th>Attribute</th><th>Price</th><th>Unit count</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <!-- / Info table -->
            </div>
        </div>
        <!-- /5. $CONTROLS -->
    </div>
</div>


<script>
$(document).ready(function () {
	$("#fruit_id").select2({placeholder: "Select Fruit",allowClear: true});
	$("#brand_id").select2({placeholder: "Select Brand",allowClear: true});
	$("#attr_id").select2({placeholder: "Select Attribute",allowClear: true});
	$("input[name='unit_price_s']").on('change',function(){
		var radVal = $(this).val();
		var str;
		if(radVal==0){
			str = '<div class="col-sm-12"><input type="text" class="form-control" name="unit_price1" id="unit_price1" value="" placeholder="Price" required /></div>';
		}
		else{
			str = '<div class="col-sm-6"><input type="text" class="form-control priceonly" name="unit_price1" id="unit_price1" value="" placeholder="Min Price" required /></div><div class="col-sm-6"><input type="text" class="form-control" name="unit_price2" placeholder="Max Price" required /></div>';
		}
		$("#priceDiv").html(str);
	});
	
	$("#loadList").on('click', function (e) {
		
		$.ajax({
				type: 'post',
				url: '<?php echo base_url() . "mapping/getList"; ?>',
				data: {},
				dataType: "html",
				success: function (res) {
					$("#mapping_list tbody").html(res);
				}
			});
	});
	$("#loadList").trigger('click');
	$("#frm").on('submit', function (e) {
		$("#add_mapping").attr("disabled");
		if($("#mapping_id").length>0)
		var mapping_id = $("#mapping_id").val();
		else
		var mapping_id = 0;
		$.ajax({
			type: 'post',
			url: '<?php echo base_url() . "mapping/save_mapping/"; ?>'+mapping_id,
			data: new FormData(this),
			dataType: "json",
			processData: false,
			contentType: false,
			success: function (res) {
				//console.log(res);
				$("#frm")[0].reset();
				$("#add_mapping").removeAttr("disabled");
				$("#errorDiv").html(res.msg);
				$("#loadList").trigger('click');
				$("#fruit_id").val('').change();
				$("#brand_id").val('').change();
				$("#attr_id").val('').change();
				$("input[name='unit_price_s']").trigger('change');
				if(mapping_id!=0){
					$("#add_mapping").val('Add Mapping');
				}
			},
			error: function () {
				$("#add_mapping").removeAttr("disabled");
				$("#errorDiv").html('Something is wrong, please try again.');
			}
		});
		return false;
	});
	$("#mapping_list tbody").on("click",".editMapping", function(e){	
		var editId = $(this).attr('id');
		
		$("#errorDiv").html('<img src="<?=base_url()?>admin_asset/assets/images/loader.gif">');
		$.ajax({
			type: 'post',
			url: '<?php echo base_url() . "mapping/edit"; ?>',
			data: {editId:editId},
			dataType: "json",
			success: function (res) {
				if(res.status=='success'){
					//console.log(res.result['id']);
					$("#fruit_id").val(res.result['fruit_id']).change();
					$("#brand_id").val(res.result['brand_id']).change();
					$("#attr_id").val(res.result['attr_id']).change();
					//$("input [name='unit_price_s']").val(res.result['unit_price_s']).checked;
					//$("input:radio[name='unit_price_s']").filter('[value="'+res.result['unit_price_s']+'"]').prop('checked', true).change();
					//var $input = $( ".ui-popup-container" ).find( "input" ).eq(2);
					var unit_price_s = res.result['unit_price_s'];
					$("input:radio[name='unit_price_s']").filter('[value="'+unit_price_s+'"]').prop('checked', true); 
					
					if(unit_price_s==0){
						str = '<div class="col-sm-12"><input type="text" class="form-control" name="unit_price1" value="'+res.result['unit_price1']+'" placeholder="Price" required /></div>';
					}
					else{
						str = '<div class="col-sm-6"><input type="text" class="form-control priceonly" name="unit_price1" value="'+res.result['unit_price1']+'" placeholder="Min Price" required /></div><div class="col-sm-6"><input type="text" class="form-control priceonly" name="unit_price2" placeholder="Max Price" required value="'+res.result['unit_price2']+'" /></div>';
					}
					$("#priceDiv").html(str);
					$("input:text[name='unit_count']").val(res.result['unit_count']);	
					$("#subtype").val('edit');
					$("#subtype").after('<input id="mapping_id" value="'+res.result['id']+'" type="hidden">');	
					$("#add_mapping").val('Edit Mapping');
					$("#errorDiv").html('');
				}
				//console.log(res)
				//$("#mapping_list tbody").html(res);
			},
			error: function () {
				$("#add_mapping").removeAttr("disabled");
				$("#errorDiv").html('Something is wrong, please try again.');
				$("#errorDiv").html('');
			}
		});
	});
	$("#mapping_list tbody").on("click",".deleteMapping", function(e){	
		var deleteId = $(this).attr('id');
		$("#errorDiv").html('');
		var r = confirm("Do you really want to delete this mapping?");
		if (r == true) {
			$.ajax({
				type: 'post',
				url: '<?php echo base_url() . "mapping/delete"; ?>',
				data: {deleteId:deleteId},
				dataType: "json",
				success: function (res) {
					if(res.status=='success'){
						$("#errorDiv").html(res.msg);
						$("#loadList").trigger('click');
					}
					else{
						$("#errorDiv").html(res.msg);
					}
				},
				error: function () {
					$("#add_mapping").removeAttr("disabled");
					$("#errorDiv").html('Something is wrong, please try again.');
				}
			});
		}		
	});
});
</script>

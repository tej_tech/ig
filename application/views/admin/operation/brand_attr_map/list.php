<?php
if(sizeof($mapping)>0){
	$cnt=1;
	foreach ($mapping as $m) { ?>
        <tr id="brandremove_<?php echo $m['id']; ?>">
            <td><?php echo $cnt; ?></td>
            <td><?php echo stripslashes($m['name']) ?></td>
            <td><?php echo stripslashes($m['b_name']) ?></td>
            <td><?php echo stripslashes($m['attr_name']) ?></td>
            <td><?php if($m['unit_price_s']==1) echo $m['unit_price1'].' to '.$m['unit_price2']; else echo $m['unit_price1']; ?></td>
            <td><?php echo $m['unit_count'] ?></td>
            <td><!--data-toggle="modal" data-target="#myModal1" -->
                <a href="javascript:void(0)" title="Edit" id="upbr_<?php echo $m['id']; ?>" class="btn btn-xs btn-outline editMapping"><i class="fa fa-pencil"></i></a>
                <a href="javascript:void(0)" title="Delete" id="bdel_<?php echo $m['id']; ?>" class="btn brand_delete btn-xs btn-outline deleteMapping"><i class="fa fa-times"></i></a>
            </td>
        </tr>
    <?php
	$cnt++;
	}
}
else{
	echo '<tr><td colspan="2">There are not any mapping records</td></tr>';	
}
?>
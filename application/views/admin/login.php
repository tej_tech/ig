<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Sign In - IG</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

        <!-- Open Sans font from Google CDN -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">

        <!-- Pixel Admin's stylesheets -->
        <link href="<?php echo $this->config->item('admin_css') ?>bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->config->item('admin_css') ?>pixel-admin.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->config->item('admin_css') ?>pages.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->config->item('admin_css') ?>rtl.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->config->item('admin_css') ?>themes.min.css" rel="stylesheet" type="text/css">

        <!--[if lt IE 9]>
                <script src="assets/javascripts/ie.min.js"></script>
        <![endif]-->

    </head>

    <body class="theme-default page-signin-alt">
        <div class="signin-header">
            <a href="#" class="logo">
                <strong>IG International</strong>
            </a> <!-- / .logo -->

        </div> <!-- / .header -->

        <h1 class="form-header">Sign in to your Account</h1>


        <!-- Form -->
        <form action="index.html" id="signin_form" class="panel">
            <div class="form-group">
                <input type="text" name="signin_email_id" id="email_id" class="form-control input-lg" placeholder="Email-id">
                <span id="email_err"></span>
            </div> <!-- / Username -->

            <div class="form-group signin-password">
                <input type="password" name="signin_password" id="password" class="form-control input-lg" placeholder="Password">
                <span id="pass_err"></span>
            </div> <!-- / Password -->

            <div class="form-actions">
                <input type="submit" value="Sign In" id="login_btn" class="btn btn-primary btn-block btn-lg">
                <span id="all_err"></span>
            </div> <!-- / .form-actions -->
        </form>
        <!-- / Form -->


        <!-- Get jQuery from Google CDN -->
        <!--[if !IE]> -->
        <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js">' + "<" + "/script>");</script>
        <!-- <![endif]-->
        <!--[if lte IE 9]>
                <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
        <![endif]-->

        <script src="<?php echo $this->config->item('admin_js') ?>bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                var email = $("#email_id");
                var pass = $("#password");
                email.keyup(check_email);
                email.blur(check_email);
                pass.keyup(check_pass);
                pass.blur(check_pass);

                function check_email() {
                    if ($("#email_id").val().length == 0) {
                        $("#email_err").parent().addClass('has-error');
                        $("#email_err").html('<font color="red">Required field</font>');
                        return false;
                    } else {
                        $("#email_err").parent().removeClass('has-error');
                        $("#email_err").html('<font color="green">Done</font>');
                        return true;
                    }
                }

                function check_pass() {
                    if ($("#password").val().length == 0) {
                        $("#pass_err").parent().addClass('has-error');
                        $("#pass_err").html('<font color="red">Required field</font>');
                        return false;
                    } else {
                        $("#pass_err").parent().removeClass('has-error');
                        $("#pass_err").html('<font color="green">Done</font>');
                        return true;
                    }
                }
                $("#signin_form").on('submit', function (e) {

                    if (check_email() & check_pass()) {
                        $("#login_btn").attr("disabled", "disabled")
                        $("#all_err").html('<font color="blue">Please wait...</font>');
                        $.ajax({
                            type: 'post',
                            url: '<?php echo site_url('admin/validate_login'); ?>',
                            data: new FormData(this),
                            processData: false,
                            contentType: false,
                            success: function (res) {
                                $("#login_btn").removeAttr("disabled");
                                if (res == 0) {
                                    $("#all_err").html('<font color="red">Wrong login details</font>');
                                }else{
                                    window.location.href = '<?php echo base_url() . "admin/"; ?>';
                                }
                            }
                        });
                    } else {
                        $("#all_err").html('<font color="red">Fill all fields properly</font>');
                    }
                    e.preventDefault();
                    return false;
                });
            });
        </script>

    </body>
</html>

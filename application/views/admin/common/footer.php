<div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->
<!-- Pixel Admin's javascripts -->
<script src="<?php echo $this->config->item('admin_js') ?>bootstrap.min.js"></script>
<script src="<?php echo $this->config->item('admin_js') ?>pixel-admin.min.js"></script>
<script src="<?php echo $this->config->item('admin_js') ?>common.js"></script>
<script type="text/javascript">
    init.push(function () {
        // Javascript code here
    })
    window.PixelAdmin.start(init);
</script>

</body>
</html>
<div id="main-navbar" class="navbar navbar-inverse" role="navigation">
    <!-- Main menu toggle -->
    <button type="button" id="main-menu-toggle"><i class="navbar-icon fa fa-bars icon"></i><span class="hide-menu-text">HIDE MENU</span></button>
    <div class="navbar-inner">
        <!-- Main navbar header -->
        <div class="navbar-header">
            <!-- Logo -->
            <a href="#" class="navbar-brand">IG</a>
            <!-- Main navbar toggle -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse"><i class="navbar-icon fa fa-bars"></i></button>

        </div> <!-- / .navbar-header -->

        <div id="main-navbar-collapse" class="collapse navbar-collapse main-navbar-collapse">
            <div>
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#"></a>
                    </li>
                </ul> <!-- / .navbar-nav -->

                <div class="right clearfix">
                    <ul class="nav navbar-nav pull-right right-navbar-nav">
                        <li class="nav-icon-btn nav-icon-btn-success dropdown">
                            <a href="#messages" id="pending_comment" class="dropdown-toggle" data-toggle="dropdown">
                                <input type="hidden" id="p_comment_lcount" value="0"/>
                                <span class="label d"><?php echo "5"; ?></span>
                                <i class="nav-icon fa fa-envelope"></i>
                                <span class="small-screen-text">Income messages</span>
                            </a>

                            <!-- MESSAGES -->

                            <!-- Javascript -->
                            <script>
                                init.push(function () {
                                    $('#main-navbar-messages').slimScroll({height: 250});
                                });

                               
                            </script>
                            <!-- / Javascript -->

                            <div class="dropdown-menu widget-messages-alt no-padding" style="width: 300px;">
                                <span id="wait_comment_pen">please wait</span>
                                <div class="messages-list" id="main-navbar-messages">
                                </div> <!-- / .messages-list -->
                                
                            </div> <!-- / .dropdown-menu -->
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle user-menu" data-toggle="dropdown">
                                <img src="<?php echo image_url($this->session->userdata('image_url'), 'icon') ?>" class="image_icon_session" alt="">
                                <span class="top_name"><?php echo ucwords($this->session->userdata('name')); ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Profile/Setting</a></li>
                                <li><a href="<?php echo base_url(); ?>" target="_blank">Visit website</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url('admin/logout'); ?>"><i class="dropdown-icon fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
                            </ul>
                        </li>
                    </ul> <!-- / .navbar-nav -->
                </div> <!-- / .right -->
            </div>
        </div> <!-- / #main-navbar-collapse -->
    </div> <!-- / .navbar-inner -->
</div> <!-- / #main-navbar -->

<div id="main-menu" role="navigation">
    <div id="main-menu-inner">
        <div class="menu-content top" id="menu-content-demo">
            <div class="text-bg"><span class="text-slim">Welcome,</span> <span class="text-semibold top_name"><?php echo ucwords($this->session->userdata('name')); ?></span></div>
            <div class="btn-group">
                <a href="#" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-user"></i></a>
                <a href="<?php echo base_url(); ?>" target="_blank" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-external-link"></i></a>
                <a href="<?php echo base_url('admin/logout'); ?>" class="btn btn-xs btn-danger btn-outline dark"><i class="fa fa-power-off"></i></a>
            </div>
        </div>
        <ul class="navigation">
            <li>
                <a href="<?php echo base_url('admin'); ?>"><i class="menu-icon fa fa-dashboard"></i><span class="mm-text">Dashboard</span></a>
            </li>
            <li class="mm-dropdown">
                <a href="#"><i class="menu-icon fa fa-gears"></i><span class="mm-text">Operations Master</span></a>
                <ul>
                    <li><a tabindex="-1" href="<?php echo base_url('fruits'); ?>"><i class="menu-icon fa fa-apple"></i><span class="mm-text">Fruits</span></a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('item_brand'); ?>"><i class="menu-icon fa fa-bars"></i><span class="mm-text">Brands</span></a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('brand_attr'); ?>"><i class="menu-icon fa fa-book"></i><span class="mm-text">Attributes</span></a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('sales/lists'); ?>"><i class="menu-icon fa fa-shopping-cart"></i><span class="mm-text">Sales List</span></a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('stock_management'); ?>"><i class="menu-icon fa fa-briefcase"></i><span class="mm-text">Stock management</span></a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('tab'); ?>"><i class="menu-icon fa fa-tablet"></i><span class="mm-text">Tab Master</span></a></li>
                    <li><a tabindex="-1" href="<?php echo base_url('mapping'); ?>"><i class="menu-icon fa fa-plus-square"></i><span class="mm-text">Mapping</span></a></li>
                </ul> 
            </li>
            
            <?php if ($this->session->userdata('role') == "admin") { ?>
                <li class="mm-dropdown">
                    <a href="#"><i class="menu-icon fa fa-th"></i><span class="mm-text">User Setting</span></a>
                    <ul>
                    <li>
                        <a href="<?php echo base_url('user/create'); ?>"><i class="menu-icon fa fa-user"></i><span class="mm-text">Add User</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('user/lists'); ?>"><i class="menu-icon fa fa-users"></i><span class="mm-text">Users List</span></a>
                    </li>
                        
                        <li>
                            <a tabindex="-1" href="<?php echo base_url('add_store'); ?>"><i class="menu-icon fa fa-check-square"></i><span class="mm-text">Add store</span></a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
        </ul> <!-- / .navigation -->
    </div> <!-- / #main-menu-inner -->
</div> <!-- / #main-menu -->
<!-- /4. $MAIN_MENU -->

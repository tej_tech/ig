<!DOCTYPE html>
<!--

TABLE OF CONTENTS.

Use search to find needed section.

=====================================================

|  1. $BODY                 |  Body                 |
|  2. $MAIN_NAVIGATION      |  Main navigation      |
|  3. $NAVBAR_ICON_BUTTONS  |  Navbar Icon Buttons  |
|  4. $MAIN_MENU            |  Main menu            |
|  5. $PROFILE              |  Profile              |

=====================================================

-->


<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Dashboard - TWG</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

        <!-- Open Sans font from Google CDN -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
        <!-- Pixel Admin's stylesheets -->
        <link href="<?php echo $this->config->item('admin_css') ?>bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->config->item('admin_css') ?>pixel-admin.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->config->item('admin_css') ?>widgets.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->config->item('admin_css') ?>rtl.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->config->item('admin_css') ?>pages.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->config->item('admin_css') ?>themes.min.css" rel="stylesheet" type="text/css">

        <script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js">' + "<" + "/script>");</script>

        <!--[if lt IE 9]>
                <script src="assets/javascripts/ie.min.js"></script>
        <![endif]-->
        <style>
            #demo-settings{
                display: none;
            }
        </style>
    </head>


    <!-- 1. $BODY ======================================================================================
            
            Body
    
            Classes:
            * 'theme-{THEME NAME}'
            * 'right-to-left'      - Sets text direction to right-to-left
            * 'main-menu-right'    - Places the main menu on the right side
            * 'no-main-menu'       - Hides the main menu
            * 'main-navbar-fixed'  - Fixes the main navigation
            * 'main-menu-fixed'    - Fixes the main menu
            * 'main-menu-animated' - Animate main menu
    -->
    <body class="theme-default main-menu-animated page-profile">

        <script>var init = [];</script>
        <!-- Demo script --> <script src="<?php echo $this->config->item('admin_demo') ?>demo.js"></script> <!-- / Demo script -->

        <div id="main-wrapper">
            <?php $this->load->view('admin/common/side_menu'); ?>
            <div id="content-wrapper">